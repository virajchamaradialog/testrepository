///////////////////////////////////////////////////////////////////////
//
//Up Selling Module
//Author:Kassapa Asanka
//Last Modified Date:2011-03-14
//
//Notes:Currently eleiglble for FUP Hit notifications / Dormmant Bonus
//The code "INTERNETGO" added to identify the internet card subscribers.identification is done by using the subaccount type
//
//
//
//
////////////////////////////////////////////////////////////////////////////



import java.io.*;
import java.util.*;
import java.net.*;
import java.sql.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


public class UP_Sell_Mod_Test
{

	public static void main(String args[])
	{
		String msg ="";
		FileWriter logFile;
		PrintWriter logOutput;
		FileWriter Tour_In;
		PrintWriter Tour_Out;
		DateFormat idateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date icdate = new Date();

		try
		{

			//Database Connection
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con1=DriverManager.getConnection("jdbc:oracle:thin:@(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.71.226)(PORT = 1522))(ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.71.227)(PORT = 1522)) (LOAD_BALANCE = yes)(CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = apps) ) )","taploader","taploader");
			con1.setAutoCommit(true);
			//OCSObj ocsobj = new OCSObj();
			OCSObjprov3 ocsobj3 = new OCSObjprov3();
			CGObj cg1 =new CGObj("PKGTRN");

			//Log File

			//logFile = new FileWriter("/logs/UPSELL/upsell.log" ,true);
			logFile = new FileWriter("/data2/kassapa/UPSELL/upsell_test.log" ,true);
			logOutput = new PrintWriter(logFile);
			logOutput.println(idateFormat.format(icdate) +"INFO Strating the UP Selling Application");
			logOutput.flush();

			Tour_In = new FileWriter("/data2/kassapa/UPSELL/tour_result.log" ,true);
			Tour_Out = new PrintWriter(Tour_In);

			ServerSocket s = new ServerSocket(5568); //Need to change the ussd test socket
			//ServerSocket s = new ServerSocket(5011); //Need to change the socket
			icdate = new Date();
			logOutput.println(idateFormat.format(icdate) +" INFO Ready for accepting sockets");
			logOutput.flush();

			int cnt1=0;

			while(true)
			{
				Socket incoming = s.accept( );
				Thread t = new ThreadedEchoHandler(incoming,con1 ,logOutput,ocsobj3 ,cg1 ,Tour_Out); t.start();
			}
		}
		catch(Exception	e)
		{
			e.printStackTrace();

		}
	}
}


//------------------------------------------------------------------------------------------------------------------------

class ThreadedEchoHandler extends Thread
{
	public ThreadedEchoHandler(Socket inc,Connection cn ,PrintWriter logf ,OCSObjprov3 ocsobj3 ,CGObj cg2 ,PrintWriter Tour)
	{

		ocs3 = ocsobj3;
		con =cn;
		in =inc;
		logfi = logf;
		icdate = new Date();
		idateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		idateFormat2 = new SimpleDateFormat("yyyyMMddHHmmss");
		cg =cg2;
		Tour_File=Tour;

	}
	public void run()
	{

		FileWriter ff;
		PrintWriter outwrite;
		Integer inc1 =1;
		String [] DB_values ;
		DB_values = new String[10];
		Calendar cal = Calendar.getInstance();
		Integer incc =0;
		Integer incc_2 =0;
		try
		{
			inread = new BufferedReader(new InputStreamReader(in.getInputStream()));
			out = new PrintWriter(in.getOutputStream(),true);
			String str = inread.readLine();


			if (str.indexOf("index.php")> 0) //USSD main Menu--------------------------
			{
				String	[] temp_us = str.split("=");
				String temp_2_us = temp_us[1].substring(0, temp_us[1].indexOf("&"));

				log_str = idateFormat.format(icdate)+"|New USSD Requset|"+str;
				//log_str = idateFormat.format(icdate)+"|USSD|"+temp_2_us;


				//  Integer cat = check_st(temp_2_us ,idateFormat2.format(icdate));

				//Checking the offererd items-------------------------------------------------
				String [] res = USSD_offermenu_check(temp_2_us ,idateFormat2.format(icdate),"1");
				out.print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>      ");
				out.print("<vxml application=\"TEST_MENU.vxml\">  ");
				out.print("<menu id=\"menu_1\" name=\"menu_1\"> ");
				if ( !(res[0].equals("NOK") ))
				{
					out.print("<prompt>Welcom to Dialog Upselling. Choose the offer from the below List.</prompt>    ");
				}
				else
				{
					out.print("<prompt>At the moment You are Not an Eligible Subscriber.</prompt>    ");
					log_str = log_str +"|At the moment You are Not Eligible Subscriber.";
				}
				out.print("<property name=\"inputmodes\" value=\"dtmf\"/>    ");

				if ( !(res[0].equals("NOK") ))
				{
					while(incc < res.length)
					{
						//out.print("<choice dtmf=\""+(incc+1)+"\" next=\"#4cf4cbec209f3\" hidden=\"N\">"+res[incc]+"</choice>");
						out.print("<choice dtmf=\""+(incc+1)+"\" next=\"#"+(incc+1)+"\" hidden=\"N\">"+res[incc]+"</choice>");
						incc++;
					}
				}
				else
				{
					out.print("<choice dtmf=\"99\" next=\"#end\" hidden=\"N\"> No Offers</choice>");
				}
				out.print("<catch event=\"nomatch\">  ");
				out.print("<prompt>Invalid choice. Try again.</prompt>  ");
				out.print("<goto next=\"#menu_1\"/> ");
				out.print("</catch>  ");
				out.print("<catch event=\"noinput\">  ");
				out.print("<prompt>Please try again</prompt>   ");
				out.print("<goto next=\"#menu_1\"/> ");
				out.print("</catch>  ");
				out.print("</menu>  ");

				while(incc_2 < res.length)
				{
					out.print("<form id=\""+(incc_2+1)+"\" name=\"VAR_1\">    ");
					out.print("<block name=\"oc_ActionUrl\">   ");
					out.print("<goto next=\"http_client://678_ACT_SERVER_TEST/test.php?MSISDN=%ORIG%&amp;amp;sessid=%SESSION_ID%&amp;amp;acc="+res[incc_2]+"\"/>");
					out.print("</block>");
					out.print(" <block name=\"oc_NextNodeUrl\">    ");
					out.print("<goto next=\"#end\"/>");
					out.print(" </block>  ");
					out.print("  </form>  ");
					incc_2++;
				}

				out.print("<form id=\"end\" name=\"end\"> ");
				out.print("<field name=\"oc_end\"> ");
				out.print("<prompt>Thank You</prompt> ");
				out.print("</field>");
				out.print("<filled>");
				out.print("<assign name=\"\" expr=\"oc_end\"/> ");
				out.print("<goto next=\"\"/>");
				out.print("</filled> ");
				out.print(" <catch event=\"nomatch\">");
				out.print("<prompt>Invalid choice. Try again.</prompt> ");
				out.print("<goto next=\"#end\"/> ");
				out.print("</catch> ");
				out.print("<property name=\"oc_bIsFinal\" value=\"1\"/>  ");
				out.print("</form>");

				out.print("<form id=\"endmsg\" name=\"endmsg\"> ");
				out.print("<block>");
				out.print("<if cond=\"'%ErrCode%' != '0'\">");
				out.print("<prompt>Sorry, Your request not Completed, please try again shortly.</prompt>  ");
				out.print("<else/> ");
				out.print("	<prompt>%ResponseText%</prompt> ");
				out.print("</if> ");
				out.print("</block> ");
				out.print("</form>");

				out.print("</vxml> ");


				out.flush();
				out.close();




			}
			else if (str.indexOf("test.php")> 0) //USSD CX Request----------------------
			{
				String	[] temp_us_re = str.split("=");
				String newms = temp_us_re[1].substring(0, temp_us_re[1].indexOf("&")); //MSISDN
				String  [] off = temp_us_re[3].split(" ");
				String newString = off[0].replace("%5F", "_");	//Product Name

				log_str = idateFormat.format(icdate)+"|USSD Response|"+newms;
				String [] res_test = USSD_offermenu_check(newms ,idateFormat2.format(icdate),"2");
				//ccbs(newms);

				/*
				String [] us_PID =Pakcage_Code_ussd(newString ,newms ,idateFormat2.format(icdate));
				logfi.println(idateFormat.format(icdate)+"|New USSD Requset|"+str);
				log_str = idateFormat.format(icdate)+"|USSD|"+newms;


				if(us_PID[0].equals("OK"))
				{
				us_PID[2] = us_PID[2].substring(0,19);
				us_PID[2]= us_PID[2].replaceAll("\\D", "");

				//Product Provisionning-----------------------------------------------------------
				boolean res_p_u =prod_add(newms ,us_PID[1],idateFormat2.format(icdate),us_PID[2] , out ,1);
				if (res_p_u)
				{
				boolean xx = DB_Update(us_PID[3],2,1);
				}
				else
				{
				log_str	= log_str +"|DB Update did not happen.";
				}
				}
				*/


			}
			else  //SMS Based Method------------------------------------------------------
			{

				String [] temp = null;
				String [] temp_2 =null;
				String [] temp_3 = null;
				String pkg = "";
				String expd = "";
				String para_4 = "";


				temp_2 = str.split("/");
				temp = temp_2[1].split(",");
				String num  = temp[0]; //msisdn


				if( num != null)
				{

					String ver="OCS3";

					String task = temp[1]; //Operation Type
					System.out.println("Printing task:" + task);
					if(temp.length > 2) {	  pkg = temp[2]; }  //Package Code
					if(temp.length > 3) {   expd = temp[3]; }  //Expiary date
					if(temp.length > 4) { para_4 = temp[4]; } //Parameter 4

					pkg =  pkg.trim();
					//  logfi.println(idateFormat.format(icdate)+"|New SMS Requseet|"+str);
					// Log String Start---------------------------------------------------------------
					log_str = idateFormat.format(icdate)+"|"+ver+"|SMS|"+str+"|";


					if (task.equals("REG")) // Registration for the Offer--------------------------------
					{
						//Check the validity of the request
						boolean  NA = Request_valid_step1(num,idateFormat2.format(icdate) );
						if (NA)
						{
							log_str = log_str + "|Valide CX Request";

						}
						else
						{
							log_str = log_str + "|Not a Valide Request";
						}
					}
					else if (task.equals("GET")) // Reqesting the Offer---------------------------------
					{

						//Verify Package -----------------------------------------------------------------
						String [] very = Request_valid_step2(pkg ,num ,idateFormat2.format(icdate) ,"2");
						if(very[0].equals("OK"))
						{
							very[2] = very[2].substring(0,19);
							very[2]= very[2].replaceAll("\\D", "");

							//Get Prodiuct ID-----------------------------------------------------------------
							String det = Pakcage_Code(pkg);


							//Product Provisionning-----------------------------------------------------------
							boolean res_p =prod_add(num ,det,idateFormat2.format(icdate),very[2] ,out ,2);
							if (res_p)
							{
								boolean xx = DB_Update(very[3] ,2 ,1);
							}
						}


					}
					else if (task.equals("MORE")) // 1More for FUP Hit customers.
					{
						String [] very_more = Request_valid_step2(pkg ,num ,idateFormat2.format(icdate),"1");
						boolean sms_res =ccbs(num ,very_more[3]);

					}
					else if (task.equals("DORM"))  //For CEP Dormant Offer - Product attachment
					{
						String ret_date="";
						String now_d="";
						try
						{
							Integer ex_t = Integer.valueOf(expd)	;

							Date aaa =  new Date(icdate.getTime() + ex_t*24*60*60*1000);
							ret_date =idateFormat2.format(aaa);
							if(pkg.equals("900048"))
							{
								ret_date="20370101000000";
							}
							//	now_d =idateFormat.format(icdate);



							boolean ad_pr =prod_add(num ,pkg,idateFormat2.format(icdate) ,ret_date ,out ,3 );

							/*
							if (ad_pr)
							{
							boolean bb =DB_insert(num ,"5" ,idateFormat2.format(icdate));
							}
							*/

						}
						catch(Exception e)
						{
							log_str = log_str + "|" + e;
							e.printStackTrace();
						}




					}
					else if (task.equals("DORM_ADV"))
					{
						String url1 = "http://192.168.71.225:5564/SPECIAL_ADV,"+num+",CEP_R20,40,0,XXX";
						URL str_url = new URL(url1);
						BufferedReader BR1 = new BufferedReader(new InputStreamReader(str_url.openStream()));
						String line = BR1.readLine();
						log_str = log_str+line;

						if (line.indexOf("Dear customer, you have received an advanced reload")   != -1)
						{
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7000");
						}
						else if (line.indexOf("Sorry, your request cannot be processed. You have already received the maximum allowed advanced reload amount")  != -1)
						{
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7003");
						}
						else
						{
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7004");

						}


					}
					else if(task.equals("BPOS"))
					{
						try
						{
							Boolean eligible = false;
							String  sms_txt="";
							try
							{
								FileInputStream fstream = new FileInputStream("/data2/kassapa/UPSELL/control.txt");
								DataInputStream in = new DataInputStream(fstream);
								BufferedReader br = new BufferedReader(new InputStreamReader(in));
								String str_file;

								while ((str_file = br.readLine()) != null)
								{

									if(str_file.equals(num))
									{
										log_str = log_str+"|ESender|"	;
										eligible = true;
										break;
									}


								}
								in.close();
							} catch (Exception e) {
								System.err.println(e);
							}



							String [] expd_temp = null;
							expd_temp = expd.split(" ");
							expd = expd_temp[0];
							String  mainPR;
							mainPR = ocs3.getMainProductName(expd);
							String [] mp = null;
							mp = mainPR.split(",");
							String [] bnum = null;
							bnum=para_4.split(" ");
							Boolean fac = false;
							if(eligible)
							{


								if(mp[1].equals("Bus_POS"))
								{

									if(Integer.valueOf(pkg) == 0)
									{

										String ret;

										ret = ocs3.AddRemCugGroup(expd ,"1972",1);



										if((ret.equals("Operation successfully.")  || ret.equals("The user has been added to the group.") ))
										{

											String ADD_cug;

											ADD_cug = ocs3.EditCugGroup(expd ,bnum[0],(Integer.valueOf(pkg) + 1));


											if(ADD_cug.equals("This service cannot be processed when the prepaid subscriber is in the Idle state."))
											{

												fac = ocs3.firstActivation(expd);

												log_str = log_str+"|FA|";

												if(fac)
												{

													ADD_cug = ocs3.EditCugGroup(expd ,bnum[0],Integer.valueOf(pkg));

												}

											}

											if(ADD_cug.equals("Operation successfully."))
											{
												sms_txt = sendsms(num ,"Dialog has successfully coupled "+expd+" number and "+bnum[0]+" number. Thank You.");
												log_str = log_str+pkg+"|"+ADD_cug+"|"+sms_txt;

												out.println("HTTP/1.1 200 OK");
												out.print("Content-Type: text/html\n\n");
												out.println("7000");

											}
											else
											{

												sms_txt = sendsms(num ,"Please checks with Dialog complain management team. Thank You");
												log_str = log_str+pkg+"|"+ADD_cug+"|"+sms_txt;

												out.println("HTTP/1.1 200 OK");
												out.print("Content-Type: text/html\n\n");
												out.println("7005");

											}
										}
										else
										{
											sms_txt = sendsms(num ,"Please checks with Dialog complain management team. Thank You");
											log_str = log_str+pkg+"|"+ret+"|"+sms_txt;

											out.println("HTTP/1.1 200 OK");
											out.print("Content-Type: text/html\n\n");
											out.println("7005");
										}
									}
									else if(Integer.valueOf(pkg) == 1)
									{
										String del_ret;

										del_ret = ocs3.AddRemCugGroup(expd ,"1972",2);

										if(del_ret.equals("Operation successfully.") )
										{

											sms_txt = sendsms(num ,"Dialog has successfully decoupled the number. Thank You");
											log_str = log_str+pkg+"|"+del_ret+"|"+sms_txt;

											out.println("HTTP/1.1 200 OK");
											out.print("Content-Type: text/html\n\n");
											out.println("7000");
										}
										else
										{
											sms_txt = sendsms(num ,"Please checks with Dialog complain management team. Thank You");
											log_str = log_str+pkg+"|"+del_ret+"|"+sms_txt;
											out.println("HTTP/1.1 200 OK");
											out.print("Content-Type: text/html\n\n");
											out.println("7005");
										}

									}



								}
								else
								{
									sms_txt = sendsms(num ,"Please check with Dialog complain management team. Thank You");
									log_str = log_str+"Main Product not match";

									out.println("HTTP/1.1 200 OK");
									out.print("Content-Type: text/html\n\n");
									out.println("7005");


								}

							}
							else
							{
								sms_txt = sendsms(num ,"The Sender is not an authorized Number.");

								log_str = log_str+"|NEsender|"+sms_txt;	;
								out.println("HTTP/1.1 200 OK");
								out.print("Content-Type: text/html\n\n");
								out.println("7005");

							}

						}
						catch(Exception e)
						{
							e.printStackTrace();

							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7005");


						}



					}
					else if(task.equals("INTERCARD"))
					{
						String [] item_array = new String[3];
						String [] amount_array =new String[3];
						String [] data_arr_i = new String[2];
						String [] bal_arr  = new String[20];
						String bal="";
						boolean ad_pr=false;


						String  data = getFreeamount(Integer.parseInt(pkg));
						System.out.println(data);

						if(data.equals("Error"))
						{
							log_str = idateFormat.format(icdate)+"|SMS|"+num+"|"+task+"|"+pkg+"|Wrong Request Format";
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7010");

						}
						else
						{

							Integer ck = 0;
							String  log_s ="";
							String [] data_arr_m = data.split(",");
							String exp = data_arr_m[0];

							Date a2 = new Date();
							long b = a2.getTime();
							long r1= (long)1000*60*60*24;
							Date c = new Date(b+r1);
							DateFormat dfm = new SimpleDateFormat("yyyyMMddHHmmss");
							String cd1 =dfm.format(c);


							try
							{

								//Check balance first
								if(pkg.equals("29") || pkg.equals("49") || pkg.equals("99")|| pkg.equals("39")|| pkg.equals("69"))
								{

									String infor = ocs3.getBalance(num , "2000" , (Integer.parseInt(pkg)*10000) );
									System.out.println("Get Balance:" + infor);
									if(infor.equals("OK"))
									{
										String data_value = "";
										String sms_value = "" ;
										String sms_reason = "";
										String data_reason = "";
										String refund_data_value="";

										switch(Integer.parseInt(pkg))
										{
											case 29: sms_value ="-0.75" ;data_value="-28.25"; refund_data_value="28.25"; sms_reason = "1453" ; data_reason = "1452" ;  break;
											case 49: sms_value ="-0.75" ;data_value="-48.25"; refund_data_value="48.25"; sms_reason = "1457" ; data_reason = "1456" ;  break;
											case 99: sms_value ="-7.50" ;data_value="-91.50"; refund_data_value="91.50"; sms_reason = "1461" ; data_reason = "1460" ;  break;
											case 44: sms_value ="-1.28" ;data_value="-42.72"; refund_data_value="42.72"; sms_reason = "1391" ; data_reason = "1389" ;  break;
											case 94: sms_value ="-1.28" ;data_value="-92.72"; refund_data_value="92.72"; sms_reason = "1392" ; data_reason = "1390" ;  break;
											//New Additions
											case 39: sms_value ="-0.75" ;data_value="-22.62"; refund_data_value="38.25"; sms_reason = "1455" ; data_reason = "1454" ;  break;
											case 69: sms_value ="-0.75" ;data_value="-22.62"; refund_data_value="68.25"; sms_reason = "1459" ; data_reason = "1458" ;  break;
										}


										System.out.println("ADJ VALUES : "+num+"|"+data_value+"|"+data_reason);

										String bal_data = ocs3.accAdjustReq(num,data_value,data_reason,"INTERNETCARD","41");
										
										System.out.println("accAdjustRes:" + bal_data);
										String [] bal_data_arr = bal_data.split("\\|");
										if(Integer.parseInt(bal_data_arr[4])== 0)
										{
											Thread.sleep(500);
											log_str = log_str+data_value+"="+bal_data_arr[4]+"|";
											System.out.println("Check balance");
											bal = ocs3.accAdjustReq(num,sms_value,sms_reason,"INTERNETCARD","41");
											System.out.println("Answer accAdjustReq:" + bal);
											bal_arr = bal.split("\\|");
											log_str = log_str+sms_value+"="+bal_arr[4]+"|";


											if(Integer.parseInt(bal_arr[4]) != 0)
											{
												//Refunded
												String part= ocs3.accAdjustReq(num,refund_data_value,data_reason,"INTERNETCARD","41");
												String log_str_r;

												String [] fail_part = part.split("\\|");
												if(Integer.parseInt(fail_part[4]) == 0)
												{
													log_str_r = "Data Value "+data_value+"  Refundad Succesfull";
												}
												else
												{
													log_str_r =  "Data Value "+data_value+"Refundad Fail";
												}

												log_str = log_str+ "Balance Deduction for "+sms_value+" Failed|"+bal_data_arr[4]+"|"+log_str_r+"|";

												//log_str = log_str+"Insufficent balance";
												out.println("HTTP/1.1 200 OK");
												out.print("Content-Type: text/html\n\n");
												out.println("7006");

											}
										}
										else
										{
											log_str = log_str+ "Balance Deduction for "+data_value+" Failed|"+bal_data_arr[4];
										}
									}
									else
									{
										log_str = log_str+"Insufficent balance";
										out.println("HTTP/1.1 200 OK");
										out.print("Content-Type: text/html\n\n");
										out.println("7006");
									}
								}
								else
								{
									bal = ocs3.accAdjustReq(num,"-"+data_arr_m[1],expd,"INTERNETCARD","41");
									bal_arr = bal.split("\\|");
								}
							}
							catch(Exception e)
							{
								System.out.println(e);
							}
							
							if(Integer.parseInt(bal_arr[4]) == 0)
							{
								if(pkg.equals("29") || pkg.equals("49")|| pkg.equals("99") || pkg.equals("199") )
								{
									String new_value="";
									switch(Integer.parseInt(pkg))
									{
										case 29: new_value ="18" ;break;
										case 49: new_value ="19" ;break;
										case 99: new_value ="20" ;break;
										case 199: new_value ="30" ;break;
										default: new_value ="0";break;
									}

									String topup_response =ocs3.accTopupReq(num ,new_value);
									String [] topup_array = new String[20];
									topup_array = topup_response.split("\\|");
									if(Integer.parseInt(topup_array[4]) == 0)
									{
										ad_pr = true;
										log_str = log_str+"|"+new_value+"|Operation successfully.|";
										out.println("HTTP/1.1 200 OK");
										out.print("Content-Type: text/html\n\n");
										out.println("7000");
									}
									else
									{
										ad_pr = false;
										log_str = log_str+"|"+new_value+"|Operation failed.|"+topup_array[4];
										out.println("HTTP/1.1 200 OK");
										out.print("Content-Type: text/html\n\n");
										out.println("7002");
									}
								}
								else
								{
									ad_pr =prod_add(num ,exp,dfm.format(icdate) ,cd1 ,out ,3 );
								}

								if(!ad_pr)
								{
									try
									{
										String bal_fail;

										bal_fail= ocs3.accAdjustReq(num,data_arr_m[1],expd,"INTERNETCARD","41");

										String [] fail_bal_arr = bal_fail.split("\\|");
										if(Integer.parseInt(fail_bal_arr[4]) == 0)
										{
											log_str = log_str+"Refundad Succesfull";
										}
										else
										{
											log_str = log_str+"Refundad Fail";
										}
									}
									catch(Exception e)
									{
										log_str = log_str+"Refundad Fail";
										System.out.println(e);
									}

								}

							}
							else if(Integer.parseInt(bal_arr[4]) == 7)
							{
								log_str = log_str+"Insufficent balance";
								out.println("HTTP/1.1 200 OK");
								out.print("Content-Type: text/html\n\n");
								out.println("7006");
							}
							else
							{
								log_str = log_str+"|"+bal;
								out.println("HTTP/1.1 200 OK");
								out.print("Content-Type: text/html\n\n");
								out.println("7002");
							}
						}

					}
					else if(task.equals("INTERCARDBAL"))
					{
						try
						{


							String ret_acc_data="" ;
							String res_data=""	;
							String res_data1=""	;
							String res_data2=""	;
							String res_data3=""	;
							String res_data4=""	;
							String res_data5=""	;
							String res_data6="" ;
							String res_data7="" ;
							String res_data8="" ;
							String res_data9="" ;
							String res_data10="" ;
							String res_data11="" ;
							String res_data12="" ;
							String res_data13="" ;
							String res_data14="" ;
							String res_data15="" ;
							String res_data16="" ;
							String res_data17="" ;
							String res_data18="" ;
							
              String res_data19="" ;
              String res_data20="" ;
              String res_data21="" ;
              String res_data22="" ;
              String res_data23="" ;
              String res_data24="" ;
              String res_data25="" ;
              String res_data26="" ;

							String res_LSMBB1=""	;
							String res_LSMBB2=""	;

							String ret_acc_sms=""	 ;
							String res_sms="" ;
							String ret_acc_voice="" ;
							String res_voice="" 	;
							String res_voice2="" 	;
							
							String res_sms1="";
							String res_money="";

							String [] check_array = null;
							String check_conv = ocs3.checkConvergent(num);
							check_array = check_conv.split("\\|");


							//if(true)
							if(check_array[0].equals("0"))
							{

								String account[] = {"2500","5006","5007","5001","5002","5003" , "5004" , "5005" , "5058" , "5059" , "5077" , "5078" , "5079" , "5084" ,"5085" , "5081" , "5082","4500" , "4200" , "4131" , "41313" ,"5096","5097","5098","5099","5054","5062","5063","5064","5065","5108","5100" };
								ret_acc_data = ocs3.getAccBalancefromAccTypearray(num , account);
								System.out.println("NEW : "+ret_acc_data);



								res_data1		 	= getbal(ret_acc_data ,5001);
								res_data2		 	= getbal(ret_acc_data ,5002);
								res_data3		 	= getbal(ret_acc_data ,5003);
								res_data4		 	= getbal(ret_acc_data ,5004);
								res_data5		 	= getbal(ret_acc_data ,5005);
								res_LSMBB1		= getbal(ret_acc_data ,5058);
								res_LSMBB2		= getbal(ret_acc_data ,5059);
								res_data6		 	= getbal(ret_acc_data ,5077);
								res_data7		 	= getbal(ret_acc_data ,5078);
								res_data8		 	= getbal(ret_acc_data ,5079);
								res_data9			= getbal(ret_acc_data ,5084);
								res_data10		= getbal(ret_acc_data ,5085);
								res_data11		= getbal(ret_acc_data ,5081);
								res_data12		= getbal(ret_acc_data ,5082);
								res_data		 	= getbal(ret_acc_data ,4500);
								res_sms 			= getbal(ret_acc_data ,4200);
								res_voice 		= getbal(ret_acc_data ,4131);
								res_voice2 		= getbal(ret_acc_data ,41313);
								res_data15 		= getbal(ret_acc_data ,5096);
								res_data16 		= getbal(ret_acc_data ,5097);
								res_data17 		= getbal(ret_acc_data ,5098);
								res_data18 		= getbal(ret_acc_data ,5099);
								res_data19 		= getbal(ret_acc_data ,5006);
                                
                res_data20         = getbal(ret_acc_data ,5054);
                res_data21         = getbal(ret_acc_data ,5062);
                res_data22         = getbal(ret_acc_data ,5063);
                res_data23         = getbal(ret_acc_data ,5064);
                res_data24         = getbal(ret_acc_data ,5065);
                res_data25         = getbal(ret_acc_data ,5108);
                res_data26         = getbal(ret_acc_data ,5100);
                                
								res_sms1 			= getbal(ret_acc_data ,5007);
								res_money 		= getbal(ret_acc_data ,2500);
							}
							else
							{
								if((check_array[1].equals("MAS")|| check_array[1].equals("MEM")) && check_array[2].equals("OK"))
								{
									ret_acc_data 	= ocs3.getAccBalancefromAccType(num ,"4508");  //DATA
									res_data		 	= getbal(ret_acc_data ,4508);
								}
								else if((check_array[1].equals("MAS")|| check_array[1].equals("MEM")) && check_array[2].equals("NOK"))
								{
									res_data ="You have not subscribed the sharing Mobile Broadband Package. Pls dial #107# to activate it.";
								}
							}


							if(res_data.equals("")){
								log_str = log_str+"Balance Check Succesfull"+res_data;
								out.println("HTTP/1.1 200 OK");
								out.print("Content-Type: text/html\n\n");
								out.println("7007|"+res_LSMBB1+res_LSMBB2+res_data+res_data1+res_data2+res_data3+res_data4+res_data5+res_data6+res_data7+res_data8+res_data9+res_data10+res_data11+res_data12+res_sms+res_voice+res_voice2+res_data15+res_data16+res_data17+res_data18+res_data20+res_data21+res_data22+res_data23+res_data24+res_data25+res_data26);
							}
							else if(res_data.substring(0,2).equals("0B")){
								log_str = log_str+"Balance Check Succesfull"+res_data;
								out.println("HTTP/1.1 200 OK");
								out.print("Content-Type: text/html\n\n");
								out.println("7007|"+res_LSMBB1+res_LSMBB2+res_data+res_data1+res_data2+res_data3+res_data4+res_data5+res_data6+res_data7+res_data8+res_data9+res_data10+res_data11+res_data12+res_sms+res_voice+res_voice2+res_data15+res_data16+res_data17+res_data18+res_data20+res_data21+res_data22+res_data23+res_data24+res_data25+res_data26);
							}
							else{
								String p_name="";
								boolean p_yes=false;
								p_name=ocs3.getMainProductName(num);
								String result_name[] = p_name.split(",");
								if(result_name[0].equals("0")){
									if(result_name[1].contains("99_PM") || result_name[1].contains("99_PS")){
										p_yes=true;
									}
								}
								if(p_yes){
									log_str = log_str+"Balance Check Succesfull"+res_data;
									out.println("HTTP/1.1 200 OK");
									out.print("Content-Type: text/html\n\n");
									out.println("7007|"+res_LSMBB1+res_LSMBB2+res_data1+res_data2+res_data3+res_data4+res_data5+res_data6+res_data7+res_data8+res_data9+res_data10+res_data11+res_data12+res_sms+res_voice+res_voice2+res_data15+res_data16+res_data17+res_data18+res_data19+res_sms1+res_money+res_data20+res_data21+res_data22+res_data23+res_data24+res_data25+res_data26);
								}
								else{
									log_str = log_str+"Balance Check Succesfull"+res_data;
									out.println("HTTP/1.1 200 OK");
									out.print("Content-Type: text/html\n\n");
									out.println("7007|"+res_LSMBB1+res_LSMBB2+res_data+res_data1+res_data2+res_data3+res_data4+res_data5+res_data6+res_data7+res_data8+res_data9+res_data10+res_data11+res_data12+res_sms+res_voice+res_voice2+res_data15+res_data16+res_data17+res_data18+res_data19+res_sms1+res_money+res_data20+res_data21+res_data22+res_data23+res_data24+res_data25+res_data26);
								}
							}


						}
						catch(Exception e)
						{

							log_str = log_str+"|"+e;
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7002");

						}
					}
					else if(task.equals("PRODREM"))
					{

						try
						{

							boolean ad_pr =prod_rem(num ,pkg, out );

						}
						catch(Exception e)
						{
							log_str = log_str+"|"+e;
							e.printStackTrace();
						}



					}
					else if(task.equals("IDDCAMP"))
					{
						String [] item_array = new String[3];
						String [] amount_array =new String[3];
						String [] data_arr_i = new String[2];
						String [] bal_arr  = new String[20];
						String bal="";


						String  data = getFreeamount(Integer.parseInt(pkg));
						//	System.out.println(data);

						if(data.equals("Error"))
						{
							log_str = idateFormat.format(icdate)+"|SMS|"+num+"|"+task+"|"+pkg+"|Wrong Request Format|"+str;
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7010");

						}
						else
						{

							Integer ck = 0;
							String  log_s ="";
							String [] data_arr_m = data.split(",");
							String exp = data_arr_m[0];

							Date a2 = new Date();
							long b = a2.getTime();
							long r1= (long)1000*60*60*24;
							Date c = new Date(b+r1);
							DateFormat dfm = new SimpleDateFormat("yyyyMMddHHmmss");
							String cd1 =dfm.format(c);
							//		System.out.println("Expiary Date "+cd1);


							try
							{
								String  seqN = c.getTime() +"";
								//	System.out.println(num+"|"+seqN+"|"+Double.parseDouble(data_arr_m[1])+"|"+expd+"|Deduction amount: "+data_arr_m[1]);

								//String cgres = cg.debitAccount(num,seqN,Double.parseDouble(data_arr_m[1]),expd);   //This is for the taxabal charges.
								String cgres = cg.specificdebitAccount(num,seqN,Double.parseDouble(data_arr_m[1]),expd ,data_arr_m[2]);     //This is for the non-taxabal charges.


								bal_arr = cgres.split("\\:");
							}
							catch(Exception e)
							{
								System.out.println(e);
							}

							log_str = log_str+"CG:"+bal_arr[0]+"|";
							if(Integer.parseInt(bal_arr[0]) == 0)
							{

								boolean ad_pr =prod_add(num ,exp,dfm.format(icdate) ,cd1 ,out ,4 );
								if(ad_pr)
								{
									out.println("HTTP/1.1 200 OK");
									out.print("Content-Type: text/html\n\n");
									out.println("7000");

								}

							}
							else if(Integer.parseInt(bal_arr[0]) == 8)
							{
								log_str = log_str+"Insufficent balance";
								out.println("HTTP/1.1 200 OK");
								out.print("Content-Type: text/html\n\n");
								out.println("7006");
							}
							else
							{
								log_str = log_str+"|CG Response value:"+bal_arr[0];
								out.println("HTTP/1.1 200 OK");
								out.print("Content-Type: text/html\n\n");
								out.println("7002");
							}



						}

					}
					else if(task.equals("IDDBAL"))
					{
						try
						{

							String ret_acc_sms	 ;
							String res_sms ;
							String ret_acc_voice ;
							String res_voice 	;


							String account[] = {"4202","2102" };
							ret_acc_sms = ocs3.getAccBalancefromAccTypearray(num , account);


							//ret_acc_sms	 	= ocs3.getAccBalancefromAccType(num ,"4202");  //SMS
							res_sms 			= getbal(ret_acc_sms , 4202);
							//ret_acc_voice = ocs3.getAccBalancefromAccType(num ,"2102");   //VOICE
							res_voice 		= getbal(ret_acc_sms ,2102);


							log_str = log_str+"Balance Check Succesfull";
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7007|"+res_sms+res_voice);



						}
						catch(Exception e)
						{
							log_str = log_str+"|"+e;
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7002");

						}
					}
					else if(task.equals("CXALERT"))
					{

						try
						{
							String ret = ocs3.queryQuota(num);
							String [] result_arr = ret.split(",");
							String opr = pkg;
							String val = expd;

							if( (result_arr[0] == "NO Service") && (Integer.parseInt(opr) == 0))  //View   #764#
							{
								log_str = log_str+result_arr[0]+"|OPR:"+opr;
								out.println("HTTP/1.1 200 OK");
								out.print("Content-Type: text/html\n\n");
								out.println("7011");
							}
							else if( (result_arr[0] != "NO Service") && (Integer.parseInt(opr) == 0) )  //View
							{
								log_str = log_str+"Quota Check Succesfull|OPR:"+opr;
								out.println("HTTP/1.1 200 OK");
								out.print("Content-Type: text/html\n\n");
								out.println("7007|"+result_arr[3]);

							}
							else if ( (result_arr[0] == "NO Service") && (Integer.parseInt(opr) == 1) )  //Add
							{
								ret = ocs3.addquota(num ,Long.parseLong(val) , 0 , true ,"20370101000000");
								if(ret.equals("405000000"))
								{
									log_str = log_str+result_arr[0]+"|Quota Added Successfull With Limit:"+val+"|OPR:"+opr;
									out.println("HTTP/1.1 200 OK");
									out.print("Content-Type: text/html\n\n");
									out.println("7000");
								}
								else
								{
									log_str = log_str+result_arr[0]+"|Quota Added Fail With Limit:"+val+"|OPR:"+opr+"|"+ret;
									out.println("HTTP/1.1 200 OK");
									out.print("Content-Type: text/html\n\n");
									out.println("7002");
								}
							}
							else if( (result_arr[0] != "NO Service") && (Integer.parseInt(opr) == 1) ) //Add
							{
								if(Integer.parseInt(result_arr[1]) < 3)
								{
									if( Long.parseLong(val)*10000  > Long.parseLong(result_arr[0]) )
									{
										ret = ocs3.addquota(num ,Long.parseLong(val) , 1, false  ,result_arr[2]);
										if(ret.equals("405000000"))
										{
											ret = ocs3.addquota(num ,Long.parseLong(val) , 3 , true , result_arr[2]);
											if(ret.equals("405000000"))
											{
												log_str = log_str+"|Limit changed Successfull|Total Quota Value updated|OPR:"+opr;
												out.println("HTTP/1.1 200 OK");
												out.print("Content-Type: text/html\n\n");
												out.println("7000");
											}
											else
											{
												log_str = log_str+"|Limit changed Failed|Total Quota Value updated|OPR:"+opr+"|"+ret;
												out.println("HTTP/1.1 200 OK");
												out.print("Content-Type: text/html\n\n");
												out.println("7002");
											}
										}
										else
										{
											log_str = log_str+"|Total Quota Value update Failed|Limit did not changed|OPR:"+opr+"|"+ret;
											out.println("HTTP/1.1 200 OK");
											out.print("Content-Type: text/html\n\n");
											out.println("7002");
										}

									}
									else
									{
										ret = ocs3.addquota(num ,Long.parseLong(val) , 3 , true , result_arr[2]);
										if(ret.equals("405000000"))
										{
											log_str = log_str+"|Limit Added Succesully|OPR:"+opr+"|"+ret;
											out.println("HTTP/1.1 200 OK");
											out.print("Content-Type: text/html\n\n");
											out.println("7000");
										}
										else
										{
											log_str = log_str+"|Limit changed Failed|OPR:"+opr+"|"+ret;
											out.println("HTTP/1.1 200 OK");
											out.print("Content-Type: text/html\n\n");
											out.println("7002");
										}
									}
								}
								else
								{
									log_str = log_str+"|Reached the maximum number of quotas|OPR:"+opr+"|"+ret;
									out.println("HTTP/1.1 200 OK");
									out.print("Content-Type: text/html\n\n");
									out.println("7012");
								}
							}
							else if( (result_arr[0] == "NO Service") && (Integer.parseInt(opr) == 2) )   //Delete
							{
								log_str = log_str+result_arr[0]+"|OPR:"+opr;
								out.println("HTTP/1.1 200 OK");
								out.print("Content-Type: text/html\n\n");
								out.println("7011");
							}
							else if( (result_arr[0] != "NO Service") && (Integer.parseInt(opr) == 2) )  //Delete
							{
								ret = ocs3.addquota(num ,Long.parseLong(val) , 4 , true , result_arr[2]);
								if(ret.equals("405000000"))
								{

									if((Long.parseLong(val)*10000) == Long.parseLong(result_arr[0]))
									{
										String [] temp_array = result_arr[3].split("\\|");
										int temp_k =0;
										long max=0;
										while(temp_k < temp_array.length)
										{


											if(Long.parseLong(temp_array[temp_k]) != Long.parseLong(val) )
											{
												if(Long.parseLong(temp_array[temp_k]) > max )
												max = Long.parseLong(temp_array[temp_k]);
											}
											temp_k++;
										}
										ret = ocs3.addquota(num ,max , 1, false  ,result_arr[2]);
										if(ret.equals("405000000"))
										{
											log_str = log_str+"Quota limit Deleted|Total Quota Value Updated|OPR:"+opr;
											out.println("HTTP/1.1 200 OK");
											out.print("Content-Type: text/html\n\n");
											out.println("7000");
										}
										else
										{
											log_str = log_str+"Quota limit Deleted|Total Quota Value Update Failed|OPR:"+opr;
											out.println("HTTP/1.1 200 OK");
											out.print("Content-Type: text/html\n\n");
											out.println("7002");
										}
									}
									else
									{
										log_str = log_str+"Quota limit Changed Succefgull|OPR:"+opr;
										out.println("HTTP/1.1 200 OK");
										out.print("Content-Type: text/html\n\n");
										out.println("7000");

									}

								}
								else
								{
									log_str = log_str+"Quota limit Delete Failed|OPR:"+opr;
									out.println("HTTP/1.1 200 OK");
									out.print("Content-Type: text/html\n\n");
									out.println("7002");
								}
							}
							else if (Integer.parseInt(opr) == 3)    // Deactivate
							{
								ret = ocs3.addquota(num ,Long.parseLong(val) , 2 , false , result_arr[2]);
								if(ret.equals("405000000"))
								{
									log_str = log_str+"Total Quota Deleted Sucessfully|OPR:"+opr;
									out.println("HTTP/1.1 200 OK");
									out.print("Content-Type: text/html\n\n");
									out.println("7000");
								}
								else
								{
									log_str = log_str+"Total Quota Delete Fail|OPR:"+opr;
									out.println("HTTP/1.1 200 OK");
									out.print("Content-Type: text/html\n\n");
									out.println("7002");
								}
							}

						}
						catch(Exception e)
						{
							e.printStackTrace();

							log_str = log_str+"Exception:"+e;
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7002");

						}



					}
					else if(task.equals("TOUR"))
					{

						out.println("HTTP/1.1 200 OK");
						out.print("Content-Type: text/html\n\n");
						out.println("7000|"+pkg);

						Tour_File.println(log_str);
						Tour_File.flush();

					}
					else if(task.equals("INTERNETGO"))
					{
						try
						{
							String retString;
							boolean NoInternet = false;
							int i=1;
							String acc ="";
							String log_t="";

							retString = ocs3.getAllSubaccounts(num);

							log_str = log_str+"|"+retString;

							String [] comp_acctype = retString.split(",");
							if(comp_acctype[0].equals("0"))
							{
								int k=1;
								boolean result=false;
								while(k < comp_acctype.length)
								{
									result = getIntCheck(Integer.parseInt(comp_acctype[k]));

									if(result)
									{
										log_str = log_str+"|CAPT:"+comp_acctype[k]+"|OK";
										out.println("HTTP/1.1 200 OK");
										out.print("Content-Type: text/html\n\n");
										out.println("7000");
										break;
									}
									k++;
								}

								if(!result)
								{
									log_str = log_str+"|CAPT:None|NOK";
									out.println("HTTP/1.1 200 OK");
									out.print("Content-Type: text/html\n\n");
									out.println("7013");
								}



							}
							else
							{
								log_str = log_str+"|"+retString;
								out.println("HTTP/1.1 200 OK");
								out.print("Content-Type: text/html\n\n");
								out.println("7002");

							}




						}
						catch(Exception e)
						{
							log_str = log_str+"|"+e;
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7002");

						}




					}
					else if(task.equals("IMEIBAL"))
					{
						try
						{

							String ret_acc_data	 ;
							String res_data ;

							String ret_acc_data5080	 ;
							String res_data5080 ;

							String ret_acc_voice	 ;
							String res_voice ;

							String ret_acc_sms	 ;
							String res_sms ;


							String wording="You have no remaining quota left in your account";

							String account[] = {"4500","5080" , "4131" , "4200" };

							ret_acc_data	 	= ocs3.getAccBalancefromAccTypearray(num ,account);  //data
							res_data 			= getbal(ret_acc_data , 4500);


							ret_acc_data5080	 	= ocs3.getAccBalancefromAccTypearray(num ,account);  //data
							res_data5080 			= getbal(ret_acc_data5080 , 5080);


							ret_acc_voice	 	= ocs3.getAccBalancefromAccTypearray(num ,account);  //Voice
							res_voice 			= getbal(ret_acc_voice , 4131);

							ret_acc_sms	 	= ocs3.getAccBalancefromAccTypearray(num ,account);    //SMS
							res_sms 			= getbal(ret_acc_sms , 4200);



							if(res_data.indexOf("Expire") > 0 || res_data5080.indexOf("Expire") > 0)
							{
								wording="You have";
							}

							log_str = log_str+"Balance Check Succesfull";
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7007|"+wording+" "+res_data+res_data5080+res_voice+res_sms);



						}
						catch(Exception e)
						{
							log_str = log_str+"|"+e;
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7002");

						}
					}
					else if(task.equals("GENBAL"))
					{

						try
						{
							String subaccuntList	 ;
							String [] account_array ;
							String [] return_str ;
							String res_genaral ="" ;

							String [] pksg = pkg.split("&");
							List pkgs = Arrays.asList(pksg);

							subaccuntList	 	= ocs3.getAllSubaccountsWithBoundProd(num , "");
							account_array = subaccuntList.split(",");
							String allpkgs = "#";
							String allpkgs_browser = "HTTP";


							for(int k=1 ;k <account_array.length ;k++)
							{

								return_str = account_array[k].split("\\|");

								//System.out.println(account_array[k]);
								if(pkgs.contains(return_str[0]) || pkgs.contains(allpkgs) || pkgs.contains(allpkgs_browser) )
								{
									res_genaral = res_genaral + "acc_type="+return_str[0]+",product_id="+return_str[1]+",qtabal="+return_str[2]+",expiredate="+return_str[3]+"#";
								}
							}



							log_str = log_str+"|"+pkg+"|"+expd+"| Genaral Balance Check Succesfull";
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("70077|"+res_genaral);

						}
						catch(Exception e)
						{
							e.printStackTrace();
							log_str = log_str+"|"+e;
							out.println("HTTP/1.1 200 OK");
							out.print("Content-Type: text/html\n\n");
							out.println("7002");

						}


					}

					else
					{
						log_str = idateFormat.format(icdate)+"|SMS|"+num+"|"+task+"|"+pkg+"|Wrong Request Format"+str;
						out.println("HTTP/1.1 200 OK");
						out.print("Content-Type: text/html\n\n");
						out.println("7010");
					}

				}
			}

		}
		catch (Exception e )
		{
			e.printStackTrace();
		}




		finally {
			try {
				logfi.println(log_str);
				logfi.flush();
				in.close();


			}
			catch (Exception e2)
			{
				e2.printStackTrace();
			}
		}

	}


	private String Pakcage_Code( String pkg)
	{
		String  value_set ="";
		ResultSet rset ;
		ResultSet rset_2 ;
		try
		{
			Statement stmt = con.createStatement();
			rset = stmt.executeQuery("SELECT * from  bn_detail where OFFER_ID="+Integer.parseInt(pkg));

			while(rset.next())
			{
				value_set=	rset.getString("PR_ID");  //Product ID
			}
			stmt.close();


		}
		catch (Exception e)
		{
			e.printStackTrace();
			value_set="NO";
		}

		return value_set;
	}

	private String[] Request_valid_step2( String pk ,String msi ,String exp, String s_s )
	{
		String [] pkg_val= null;
		Integer inc =0;
		String temp_s;
		ResultSet rset ;
		ResultSet rset_2 ;
		String OID="";
		String [] dd= new  String[6];

		try
		{
			Statement stmt2 = con.createStatement();
			rset = stmt2.executeQuery("SELECT * from  bn_data where MSISDN="+msi+" and S_STATUS="+s_s+" and B_STATUS=0 and CATAGORY=0 and EXP_DATE >= to_date('"+exp+"','yyyymmddhh24miss')");
			if(rset.next())
			{
				dd[1] 	=	rset.getString("OFFER_ID");  //offer_id
				dd[2] 	= rset.getString("EXP_DATE");  //EXP Date
				dd[3] 	= rset.getString("NUM");       //EXP Date
				dd[4] 	= rset.getString("B_STATUS");  //EXP Date
				dd[5] 	= rset.getString("S_STATUS");  //EXP Date

				rset.close();
				stmt2.close();
			}
			else
			{
				dd[0] ="NOK";
				log_str = log_str+"|Not an Eligible Subscriber|";
				return dd;
			}

			//	System.out.println(dd[0]+"|"+dd[1]+"|"+dd[2]+"|"+dd[3]+"|"+dd[4]+"|"+dd[5]);
			pkg_val = dd[1].split(",");

			while( inc < pkg_val.length )
			{
				temp_s = pkg_val[inc].trim();
				if (temp_s.equals(pk))
				{
					dd[0] ="OK";
					log_str = log_str+"|Valid Package Request|";
					return dd;
				}
				inc++;
			}



		}
		catch (Exception e)
		{
			dd[0] ="NOK";
			e.printStackTrace();
			log_str = log_str+"|Request_valid_step_2 Exception";
			return dd;
		}

		dd[0] ="NOK";
		log_str = log_str+"|Offer requested is not valid";
		return dd;
	}

	private boolean Request_valid_step1( String msi ,String exp )
	{
		String [] pkg_val= null;
		Integer inc =0;
		String temp_s;
		ResultSet rset ;
		ResultSet rset_2 ;
		String [] dd= new  String[4];
		String  s_tt ="";
		String  nn="";

		try
		{
			Statement stmt2 = con.createStatement();
			rset = stmt2.executeQuery("SELECT * from  bn_data where MSISDN="+msi+" and B_STATUS=0 and S_STATUS=1 and CATAGORY=0 and EXP_DATE >= to_date('"+exp+"','yyyymmddhh24miss')");
			if(rset.next())  //Need to check whether the duplicates allow------------------
			{
				String  offer 	=	rset.getString("OFFER_ID");  //offer_id
				nn 			= rset.getString("NUM");  //offer_id

				pkg_val = offer.split(",");
				while( inc < pkg_val.length )
				{
					temp_s = pkg_val[inc].trim();
					rset_2 = stmt2.executeQuery("SELECT * from  bn_detail where OFFER_ID ="+Integer.parseInt(temp_s));
					if (rset_2.next())
					{
						s_tt = s_tt + rset_2.getString("S_TEXT")+"\n";  //offer_id
					}
					inc++;
					rset_2.close();


				}

			}
			else
			{
				log_str = log_str+"Not eligible for the current Bonus";
				//Send SMS Required....??????
				return false;
			}

			rset.close();
			stmt2.close();

			String res_str =	sendsms(msi ,s_tt);
			if (res_str.equals("OK"))
			{
				boolean x =DB_Update(nn ,2 , 0);
				if (x) { return true;}
				else
				{
					return false;
				}
			}
			else 	{	return false; }



		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}

	}

	private boolean prod_add(String msisdn ,String PID , String efectd  ,String xpird ,PrintWriter oo , Integer c_p)
	{
		Integer cnt =0;
		String ss="";



		while (cnt < 3)
		{
			try
			{
				String add_prd;

				add_prd = ocs3.subscribeApendantProductWithExpEffectTime(msisdn, PID ,efectd ,xpird ,true);




				if (add_prd.equals("Operation succeeded.") || add_prd.equals("Operation successfully.") )
				{
					log_str = log_str+"PID:"+PID+"|"+add_prd;

					if (c_p == 1)
					{
						oo.print("Thank You! Enjoy Dialog Services");
						oo.flush();
					}
					else if (c_p == 2)
					{
						ss =sendsms(msisdn,"Thank You! Enjoy Dialog Services");
					}
					else if(c_p == 4)
					{
						log_str = log_str;
					}
					else
					{
						out.println("HTTP/1.1 200 OK");
						out.print("Content-Type: text/html\n\n");
						out.println("7000");
					}
					return true;
				}
				else if ( (add_prd.indexOf("repeated subscription") > 0 )  || (add_prd.indexOf("subscribed to repeatedly") > 0 ))
				//	else if (add_prd.equals("does not support repeated subscription") || add_prd.equals("The offer "+PID+" cannot be subscribed to repeatedly"))
				{
					log_str = log_str+"PID:"+PID+"|Product Already Appendant";

					if (c_p == 1)
					{
						oo.print("You have ALready Subscribed with the offer");
						oo.flush();
					}
					else if (c_p == 2)
					{
						ss =sendsms(msisdn,"You have ALready Subscribed with the offer");
					}
					else
					{
						out.println("HTTP/1.1 200 OK");
						out.print("Content-Type: text/html\n\n");
						out.println("7001");
					}

					return false;
				}
				else if (add_prd.equals("The balance of user is insufficient") || add_prd.equals("The account balance is insufficient."))
				{
					log_str = log_str+"PID:"+PID+"|"+add_prd;

					if (c_p == 1)
					{
						oo.print("You have ALready Subscribed with the offer");
						oo.flush();
					}
					else if (c_p == 2)
					{
						ss =sendsms(msisdn,"You have ALready Subscribed with the offer");
					}
					else
					{
						out.println("HTTP/1.1 200 OK");
						out.print("Content-Type: text/html\n\n");
						out.println("7006");
					}

					return false;
				}
				else
				{
					log_str = log_str+"PID:"+PID+"|Product Appendant Fail|"+add_prd;

					if (c_p == 1)	{
						oo.print("System Error Occured, while processing");
						oo.flush();
					}
					else if (c_p == 2)
					{
						ss =sendsms(msisdn,"System Error Occured, while processing");
					}
					else
					{
						out.println("HTTP/1.1 200 OK");
						out.print("Content-Type: text/html\n\n");
						out.println("7002");
					}

					return false;
				}


			}
			catch(Exception e)
			{
				cnt++;
				e.printStackTrace();
				if (cnt >=3)
				{
					log_str = log_str+"Product ID "+PID+"|Operation Unsucessful|Attempt"+cnt+"|"+e; 		//Log String---------------------------
					out.println("HTTP/1.1 200 OK");
					out.print("Content-Type: text/html\n\n");
					out.println("7002");

					return false;
				}
			}

		}
		return false;

	}

	private String sendsms(String num ,String msg)
	{
		String Result="NOTOK";
		String msg1 = msg;
		try
		{
			Socket SMSC = new Socket("192.168.71.60",4444);
			BufferedReader FromSMSC = new BufferedReader (new InputStreamReader(SMSC.getInputStream()));
			PrintWriter ToSMSC = new PrintWriter(SMSC.getOutputStream(),true);
			ToSMSC.println(num+"#"+msg1+"#8383#");
			Result= FromSMSC.readLine();
			ToSMSC.println("bye");
			SMSC.shutdownOutput();
			SMSC.close();
			return Result;

		}
		catch (Exception e)
		{

			logfi.println(idateFormat.format(icdate) +"|"+inc+"|Exception|"+e);
			logfi.flush();
			return Result;
		}


	}

	private boolean DB_Update(String r_num ,Integer s_n ,Integer b_n)
	{
		try
		{
			//System.out.println(r_num);
			Statement stmt3 = con.createStatement();
			int kk = stmt3.executeUpdate("UPDATE bn_data SET S_STATUS="+s_n+",B_STATUS="+b_n+" where num ="+r_num);
			stmt3.close();

			log_str = log_str+"|Data DB Updated"; 		//Log String---------------------------

			return true;
		}
		catch (Exception e)
		{
			log_str = log_str+"|DB ERROR|DB S_Status:"+s_n+"|B_Status:"+b_n+" Fail|"; 		//Log String---------------------------
			return false;
		}
	}

	private boolean DB_insert(String ms ,String pi ,String add_d )
	{
		try
		{
			//	System.out.println(add_d+"|"+exp_d);
			Statement stmt_ins = con.createStatement();
			//int kk = stmt_ins.executeUpdate("INSERT INTO bn_data(NUM,ADD_DATE,MSISDN,OFFER_ID,EXP_DATE,B_STATUS,S_STATUS,CATAGORY)VALUES(BN_DATA_seq.nextval,to_date('"+add_d+"','yyyymmddhh24miss') ,'"+ms+"','5',to_date('"+exp_d+"','yyyymmddhh24miss'),1,0,2)");
			int kk = stmt_ins.executeUpdate("INSERT INTO  bn_log(NUM,CUR_DATE,MSISDN,OFFER_ID,STATUS_ID,OFFER_S ,SMS_S)VALUES( BN_log_seq.nextval,to_date('"+add_d+"','yyyymmddhh24miss'),'"+ms+"','"+pi+"',2,2,2)");
			stmt_ins.close();

			log_str = log_str+"|LOG DB Updated"; 		//Log String---------------------------

			return true;
		}
		catch (Exception e)
		{
			log_str = log_str+"|DB ERROR|"+e; 		//Log String---------------------------
			return false;
		}
	}

	private String[] USSD_offermenu_check( String msi ,String exp ,String s_st)
	{
		String [] pkg_val= null;
		String [] res_det= null;
		String [] dd_us = new String[3];
		String temp_s;
		ResultSet rset ;
		ResultSet rset2 ;
		String  s_tt ="";
		String  nn="";
		Integer inc2 =0;
		boolean xx =false;
		try
		{
			Statement stmt2 = con.createStatement();
			rset = stmt2.executeQuery("SELECT * from  bn_data where MSISDN="+msi+" and B_STATUS=0 and S_STATUS=1 and CATAGORY=0 and to_char(ADD_DATE ,'yyyymmdd') LIKE '"+exp.substring(0,8)+"' and EXP_DATE >= to_date('"+exp+"','yyyymmddhh24miss')");
			if(rset.next())  //Need to check whether the duplicates allow------------------
			{
				dd_us[1] 	=	rset.getString("OFFER_ID");  //offer_id
				dd_us[0]  = "NOK";
				dd_us[2]  = rset.getString("NUM");  //offer_id




				if (s_st.equals("1"))
				{
					pkg_val = dd_us[1].split(",");

					while( inc2 < pkg_val.length )
					{
						temp_s = pkg_val[inc2].trim();
						rset2 = stmt2.executeQuery("SELECT * from  bn_detail where OFFER_ID ="+Integer.parseInt(temp_s));
						if (rset2.next())
						{
							nn =  rset2.getString("DESCRIPTION") +","+nn;  //offer_id
						}
						inc2++;
						rset2.close();
					}
					res_det =nn.split(",");
					//	xx =DB_Update(dd_us[2] ,2 , 0);
					xx =true;
				}
				else if (s_st.equals("2"))
				{
					xx = ccbs(msi , dd_us[2]);
					//	xx =DB_Update(dd_us[2] ,2 , 1);
				}



				if (xx)
				//if (true)
				{
					rset.close();
					stmt2.close();
					return res_det;
				}
				else
				{
					rset.close();
					stmt2.close();
					log_str = log_str+"|Data Base Update Fail";
					return dd_us;
				}
			}
			else
			{
				//Send SMS Required....??????
				dd_us[0]  = "NOK";
				return dd_us;
			}



		}
		catch (Exception e)
		{
			e.printStackTrace();
			return dd_us;
		}

	}

	private String[] Pakcage_Code_ussd( String pkg_n ,String msisd ,String dd)
	{
		String  [] value_ussd = new String[4];
		ResultSet rset ;
		ResultSet rset_2 ;
		try
		{
			Statement stmt = con.createStatement();
			rset = stmt.executeQuery("SELECT * from  bn_detail where DESCRIPTION='"+pkg_n+"'");
			if(rset.next())
			{
				value_ussd[1]=	rset.getString("PR_ID");  //Product ID
			}

			rset.close();


			rset_2 =stmt.executeQuery("SELECT * from  bn_data where MSISDN="+msisd+"  and CATAGORY=1 and EXP_DATE >= to_date('"+dd+"','yyyymmddhh24miss')");
			if (rset_2.next())
			{
				value_ussd[2]=	rset_2.getString("EXP_DATE");  //Exp date
				value_ussd[3]=	rset_2.getString("NUM");  //NUM
			}

			rset_2.close();
			stmt.close();
			value_ussd[0]="OK";

		}
		catch (Exception e)
		{
			e.printStackTrace();
			value_ussd[0]="NOK";
		}

		return value_ussd;
	}

	private Integer check_st(String ms ,String dd)
	{
		ResultSet rset ;
		Integer re = 3;

		try
		{
			Statement stmt = con.createStatement();
			rset = stmt.executeQuery("SELECT * from bn_data where MSISDN="+ms+"and EXP_DATE >= to_date('"+dd+"','yyyymmddhh24miss')");
			if(rset.next())
			{
				re  =  Integer.parseInt(rset.getString("CATAGORY"));
			}

			rset.close();
			stmt.close();
			return re;

		}
		catch(Exception e){
			e.printStackTrace();
			return 3;

		}
	}

	private boolean  ccbs(String MSISDN , String db_nn)
	{
		String outPut="not ok";
		//	String MSISDN ="772300008";

		String url = "jdbc:oracle:thin:@(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = 172.26.1.7)(PORT = 1521))(ADDRESS = (PROTOCOL = TCP)(HOST = 172.26.1.5)(PORT = 1521))(LOAD_BALANCE = no)(FAILOVER=ON)) (CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME = CAM)(FAILOVER_MODE = (TYPE = SELECT)(METHOD = BASIC)(RETRIES = 5)(DELAY = 2))))";

		try {

			Connection con = getConnection(url,"app2","app234");
			try{

				CallableStatement cs;
				cs= con.prepareCall("{? = call Pkg_CRM_FUP_Extending.EXTEND_FUP_1More( ? , ? , ?)}");
				cs.setString(2, MSISDN);
				cs.setString(3, "UPSELL");
				cs.registerOutParameter(1, Types.VARCHAR);
				cs.registerOutParameter(4, Types.VARCHAR);
				cs.execute();
				outPut = cs.getString(1);
				String aaa = cs.getString(4);
				String ss = sms_ref(aaa);
				//Boolean r =DB_Update(MSISDN,1,2);
				log_str = log_str+"|"+aaa; 		//Log String---------------------------
				sendsms(MSISDN ,ss);
				boolean yy;
				boolean yy2;
				if(aaa.equals("wk_sms1"	))
				{
					yy =DB_Update(db_nn ,2 , 1);
					yy2 =DB_LOG(MSISDN ,"3");
					log_str = log_str+"|Offer Successfull"; 		//Log String---------------------------
				}
				else
				{

					yy = true;
					log_str  = log_str +"|Offer Denied";
				}

				//	System.out.println("CCBS OutPut: "+outPut+"- "+aaa);
				cs.close();
				con.close();
				return yy;
			}
			catch(Exception e)
			{
				System.out.println("excp 1:"+e);
				return  false;
			}
		}
		catch(Exception e)
		{
			System.out.println("excp 2:"+e);
			return  false;
		}


	}
	private Connection getConnection(String iUrl,String iUser,String iPass)
	{
		Connection con =null;

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch(java.lang.ClassNotFoundException e) {
			System.err.print("ClassNotFoundException: ");
			System.err.println(e.getMessage());
			e.printStackTrace();
		}

		try {
			con = DriverManager.getConnection(iUrl,iUser, iPass);
		} catch(SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
			ex.printStackTrace();
		}

		return con;
	}

	public String sms_ref(String comp_ref)
	{
		if (comp_ref.equals("wk_sms2"	)      )  {  return "Dear Customer, We are unable to process your request due to a technical error. Please try again shortly.";}
		else if (comp_ref.equals("wk_sms3"	))  {  return "Dear Customer, Speed check extension is not available for your package. It is available only for Dialog Mobile Broadband unlimited packages.";}
		else if (comp_ref.equals("wk_sms10") ) {  return "Dear Customer, This speed check extension is not available for your package.";}
		else if (comp_ref.equals("wk_sms1"	)) { return "Dear Customer, Your request for speed check extension is successful. You will be charged Rs 300/- + taxes for the next 1GB of usage.";}
		else if (comp_ref.equals("wk_sms4"	)) { return "Dear Customer. You have already subscribed to extend the speed check limit applicable for your Dialog Mobile Broadband package."; }
		else if (comp_ref.equals("wk_sms5"	)) { return "Dear Customer, We are unable to process your request due to a technical error. Please try again shortly."; }
		else if (comp_ref.equals("wk_sms7"	)) { return "Dear Customer, You have not reached the speed check extension limit on your Dialog Mobile Broadband package.";}
		else return "Request Cannot be proceesed now.";


	}

	private boolean DB_LOG(String ms  ,String pi)
	{
		Date nowdate = new Date();

		try
		{

			Statement stmt_3 = con.createStatement();
			int kk = stmt_3.executeUpdate("INSERT INTO  bn_log(NUM,CUR_DATE,MSISDN,OFFER_ID,STATUS_ID,OFFER_S ,SMS_S)VALUES( BN_log_seq.nextval,to_date('"+idateFormat2.format(nowdate)+"','yyyymmddhh24miss'),'"+ms+"','"+pi+"',0,1,2)");
			stmt_3.close();

			log_str = log_str+"|Log DB Updated"; 		//Log String---------------------------

			return true;
		}
		catch (Exception e)
		{
			log_str = log_str+"|LOB DB ERROR"; 		//Log String---------------------------
			return false;
		}
	}

	private String addDaysToDate(String date, int daysToAdd)throws Exception
	{
		Date todayDate = new Date();
		DateFormat sdf = new SimpleDateFormat("yyyymmdd");
		String strDate = sdf.format(todayDate);
		Date parsedDate = sdf.parse(date);
		//System.out.println("strDate = "+strDate);
		//System.out.println("parsedDate = "+parsedDate);

		Calendar now = Calendar.getInstance();
		now.setTime(parsedDate);
		now.add(Calendar.DAY_OF_MONTH, daysToAdd);

		String da = sdf.format(now.getTime());

		//System.out.println("Inside adddate - "+sdf.format(sdf.parse(da)));

		return da;
	}
	public String  getFreeamount(int pk)
	{
		String return_pkg;

		switch(pk)
		{
			case 29: return_pkg ="20381,29," ;break;
			case 39: return_pkg ="20387,39," ;break;
			case 49: return_pkg ="20384,49," ;break;
			case 69: return_pkg ="20388,69," ;break;
			case 99: return_pkg ="20385,99," ;break;
			case 129: return_pkg ="20389,129,," ;break;
			case 199: return_pkg ="20386,199,," ;break;
			case 249: return_pkg ="20390,249,," ;break;
			case 299: return_pkg ="20188,299," ;break;
			case 98: return_pkg ="20240,98,ID098," ;break;
			case 198: return_pkg ="20241,198,ID198," ;break;
			case 598: return_pkg ="20242,598,ID598," ;break;
			case 798: return_pkg ="20274,798,DIDD798," ;break;
			case 699: return_pkg ="20191,699,699," ;break;
			case 1499: return_pkg ="20307,1499,1499," ;break;
			case 1999: return_pkg ="20308,1999,1999," ;break;
			case 2999: return_pkg ="20309,2999,2999," ;break;
			case 449: return_pkg ="20316,449,," ;break;
			case 649: return_pkg ="20317,649,2999," ;break;
			case 949: return_pkg ="20318,949,2999," ;break;
			case 1449: return_pkg ="20319,1449,2999," ;break;
			case 1949: return_pkg ="20321,1949,2999," ;break;
			case 2949: return_pkg ="20322,2949,2999," ;break;
			case 44: return_pkg ="20343,44,," ;break;
			case 94: return_pkg ="20333,94,," ;break;
			case 48: return_pkg ="20500,48,ID048," ;break;
			default: return_pkg ="Error";break;
		}

		return return_pkg;
	}
	public String getbal(String ret_acc_bal ,Integer chk_vv)
	{

		Integer k;
		String txt_sms = "";
		String txt_data = "";
		String txt_voice = "";
		String txt_money = "";
		String [] temp1;
		String [] temp2;
		long sum2;

		temp1 = ret_acc_bal.split("\\|");
		//	System.out.println("INSIDE getbal : "+chk_vv+"|"+ret_acc_bal+"|"+ret_acc_bal.length());

		for(k=1;k <temp1.length ;k ++)
		{
			temp2 = temp1[k].split(",");
			String ye = temp2[2].substring(0,4);
			String mn = temp2[2].substring(4,6);
			String dd = temp2[2].substring(6,8);
			String hh = temp2[2].substring(8,10);
			String min = temp2[2].substring(10,12);
			String ss = temp2[2].substring(12,14);
			long sum = Long.parseLong(temp2[1]);

			if(chk_vv == Integer.parseInt(temp2[0]))
			{

				//				System.out.println("INSIDE getbal : "+chk_vv+"|"+ret_acc_bal+"|"+temp1.length);

				if((chk_vv == 5006) ||(chk_vv == 5096) || (chk_vv == 5097) || (chk_vv == 5098) || (chk_vv == 5099) || (chk_vv == 4500) ||(chk_vv == 4508) || (chk_vv == 5001) || (chk_vv == 5002) || (chk_vv == 5003) || (chk_vv == 5004) || (chk_vv == 5005)|| (chk_vv == 5058)|| (chk_vv == 5059) || (chk_vv == 5077)|| (chk_vv == 5078)|| (chk_vv == 5079) || (chk_vv == 5081) || (chk_vv == 5082)|| (chk_vv == 5084)|| (chk_vv == 5085)|| (chk_vv == 5080) || (chk_vv == 5108) || (chk_vv == 5062) || (chk_vv == 5063) || (chk_vv == 5064) || (chk_vv == 5065) || (chk_vv == 5054) || (chk_vv == 5100)) //Data
				{
					sum2 = sum/1024;
					long sum_3 = sum%1024;
					String append_str ="";

					if((chk_vv == 5058) || (chk_vv == 5084) || (chk_vv == 5081)|| (chk_vv == 5096)|| (chk_vv == 5098) || (chk_vv == 5108) || (chk_vv == 5100) ) append_str="(NIGHT)";
					else if((chk_vv == 5059) ||  (chk_vv == 5085) ||  (chk_vv == 5082) || (chk_vv == 5097) || (chk_vv == 5099) || (chk_vv == 5062) || (chk_vv == 5063) || (chk_vv == 5064) || (chk_vv == 5065) || (chk_vv == 5054) ) append_str="(DAY)";

					if (sum2 < 1)
					{
						txt_data =(sum+"B "+append_str+" Expire on "+ye+"/"+mn+"/"+dd+ " "+hh+":"+min+":"+ss+"|")  + txt_data;
					}
					else if ((sum2 >= 1) &&  (sum2 < 1024) )
					{
						txt_data =(sum2+"."+sum_3+" "+append_str+" KB Expire on "+ye+"/"+mn+"/"+dd+ " "+hh+":"+min+":"+ss+"|")  + txt_data;
					}
					else
					{
						long  sum_4 = sum2 % 1024;
						sum2 = sum2 /1024;
						txt_data =(sum2+"."+sum_4+" "+append_str+" MB Expire on "+ye+"/"+mn+"/"+dd+ " "+hh+":"+min+":"+ss+"|")  + txt_data;
					}
				}
				else if( chk_vv == 4200 )  //D2D SMS
				{
					txt_sms =(sum+" D2D SMS Expire on "+ye+"/"+mn+"/"+dd+ " "+hh+":"+min+":"+ss+"|")  + txt_sms;

				}
				else if( chk_vv == 5007 )  //Local SMS - Digital Rewards
				{
					txt_sms =(sum+" Local SMS Expire on "+ye+"/"+mn+"/"+dd+ " "+hh+":"+min+":"+ss+"|")  + txt_sms;

				}
				else if(chk_vv == 4202) //IDD SMS
				{
					txt_sms =(sum+" IDD SMS Expire on "+ye+"/"+mn+"/"+dd+ " "+hh+":"+min+":"+ss+"|")  + txt_sms;

				}
				else if(chk_vv == 2102)//IDD balance in Rupees
				{
					txt_voice =("IDD Rs."+sum/10000+"."+(sum%10000)/100+" Expire on "+ye+"/"+mn+"/"+dd+ " "+hh+":"+min+":"+ss+"|")  + txt_voice;

				}
				else if(chk_vv == 2500)//DR balance in Rupees
				{
					txt_money =("Bonus Balance Rs."+sum/10000+"."+(sum%10000)/100+" Expire on "+ye+"/"+mn+"/"+dd+ " "+hh+":"+min+":"+ss+"|")  + txt_money;
					System.out.println("Balance 2500:" + txt_money);
				}
				else if( (chk_vv == 41313)  || (chk_vv == 4131)) // D2D  in minutes
				{
					txt_voice =(sum/60 +" D2D Minutes Expire on "+ye+"/"+mn+"/"+dd+ " "+hh+":"+min+":"+ss+"|")  + txt_voice;
				}
				else if(chk_vv < 100)
				{
					txt_voice = "srv=NULL,qtaName=NULL,qtabal="+sum+",expiredate="+ye+mn+dd+hh+min+ss+"#"+txt_voice;
				}
				else    //D2D minutes
				{
					txt_voice =(sum+" D2D Minutes Expire on "+ye+"/"+mn+"/"+dd+ " "+hh+":"+min+":"+ss+"|")  + txt_voice;
				}
			}
		}

		//	System.out.println("Voice resource |"+txt_voice+"|"+chk_vv);

		return txt_data+txt_sms+txt_voice+txt_money;

	}
	public boolean check_version(String msisdn)   //True if OCS 3.3
	{
		try {

			long number = Long.parseLong(msisdn);
			Integer ver = ocs3.getOCSVersion(number);
			//			System.out.println("OCS Version :"+ver);
			if(ver == 3) {return true;}
			else return false;
		}
		catch(Exception e)
		{
			return false;
		}


	}

	private boolean prod_rem(String msisdn ,String PID ,PrintWriter oo )
	{
		try
		{
			String result="";

			result = ocs3.unSubscribeApendantProduct2(msisdn , PID);

			log_str = log_str+"PID:"+PID+"|"+result;
			if (result.equals("Operation succeeded."))
			{

				out.println("HTTP/1.1 200 OK");
				out.print("Content-Type: text/html\n\n");
				out.println("7000");

				return true;
			}
			else
			{

				out.println("HTTP/1.1 200 OK");
				out.print("Content-Type: text/html\n\n");
				out.println("7008");

				return false;
			}

		}
		catch(Exception e)
		{
			out.println("HTTP/1.1 200 OK");
			out.print("Content-Type: text/html\n\n");
			out.println("7002");
			return false;
		}
	}

	public boolean  getIntCheck(int pk)
	{
		boolean return_Int=false;;

		switch(pk)
		{
			case 5001: return_Int =true ;break;
			case 5002: return_Int =true ;break;
			case 5003: return_Int =true ;break;
			case 5004: return_Int =true ;break;
			case 5005: return_Int =true ;break;
			default:   return_Int =false;break;
		}

		return return_Int;
	}





	private Socket in;
	private PrintWriter logfi;
	private	DateFormat idateFormat ;
	private	DateFormat idateFormat2 ;
	private  Date icdate;
	private OCSObjprov3 ocs3;
	private String check_str;
	private Integer check_pkg;
	private Integer inc;
	private BufferedReader inread;
	private PrintWriter out;
	private Connection con;
	private String line;
	private String log_str;
	private String log_str_head;
	private boolean is_ocs3=false;
	private CGObj cg;
	private PrintWriter Tour_File;



}