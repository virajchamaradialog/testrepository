
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.ResourceBundle;

import com.huawei.www.bme.cbsinterface.cabmgr.CABMgr;
import com.huawei.www.bme.cbsinterface.cabmgr.CABMgrServiceLocator;
import com.huawei.www.bme.cbsinterface.cashrecharge.*;
import com.huawei.www.bme.cbsinterface.cashrechargemgr.*;
import com.huawei.www.bme.cbsinterface.changeprimaryoffermgr.ChangePrimaryOfferMgr;
import com.huawei.www.bme.cbsinterface.changeprimaryoffermgr.ChangePrimaryOfferMgrServiceLocator;
import com.huawei.www.bme.cbsinterface.common.*;
import com.huawei.www.bme.cbsinterface.cugenhance.ManCUGMemberCallListRequest;
import com.huawei.www.bme.cbsinterface.cugenhance.ManCUGMemberCallListRequestCallNumberListCallNumberInfo;
import com.huawei.www.bme.cbsinterface.cugenhancemgr.CUGEnhance;
import com.huawei.www.bme.cbsinterface.cugenhancemgr.CUGEnhanceMgrServiceLocator;
import com.huawei.www.bme.cbsinterface.cugenhancemgr.ManCUGMemberCallListRequestMsg;
import com.huawei.www.bme.cbsinterface.cugenhancemgr.ManCUGMemberCallListResultMsg;
import com.huawei.www.bme.cbsinterface.cugmgr.CUG;
import com.huawei.www.bme.cbsinterface.cugmgr.CUGMgrServiceLocator;
import com.huawei.www.bme.cbsinterface.cugmgr.ManGroupMemberRequestMsg;
import com.huawei.www.bme.cbsinterface.cugmgr.ManGroupMemberResultMsg;
import com.huawei.www.bme.cbsinterface.delsubscriber.DelSubscriberRequest;
import com.huawei.www.bme.cbsinterface.delsubscribermgr.DelSubscriberRequestMsg;
import com.huawei.www.bme.cbsinterface.productoffer.*;
import com.huawei.www.bme.cbsinterface.productoffermgr.*;
import com.huawei.www.bme.cbsinterface.query.QueryBalanceRequest;
import com.huawei.www.bme.cbsinterface.querymgr.QueryBalanceRequestMsg;
import com.huawei.www.bme.cbsinterface.querymgr.QueryBalanceResultMsg;
import com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerResultMsg;
import com.huawei.www.bme.cbsinterface.querymgr.QueryMgr;
import com.huawei.www.bme.cbsinterface.querymgr.QueryMgrServiceLocator;
import com.huawei.www.bme.cbsinterface.submgrtmgr.*;
import com.huawei.www.bme.cbsinterface.newsubscriber.*;
import com.huawei.www.bme.cbsinterface.newsubscribermgr.NewSubscriberMgr;
import com.huawei.www.bme.cbsinterface.newsubscribermgr.NewSubscriberMgrServiceLocator;
import com.huawei.www.bme.cbsinterface.newsubscribermgr.NewSubscriberRequestMsg;
import com.huawei.www.bme.cbsinterface.newsubscribermgr.NewSubscriberResultMsg;
import com.huawei.www.bme.cbsinterface.notifymgr.*;
import com.huawei.www.bme.cbsinterface.adjustaccount.*;
import com.huawei.www.bme.cbsinterface.adjustaccountmgr.*;
import com.huawei.www.bme.cbsinterface.subscribe.*;
import com.huawei.www.bme.cbsinterface.subscribemgr.*;
import com.huawei.www.bme.cbsinterface.acctcreditmgr.*;
import com.huawei.www.bme.cbsinterface.managecsmgr.*;
import com.huawei.www.bme.cbsinterface.managefnmgr.*;
import com.huawei.www.bme.cbsinterface.managedualnomgr.*;
import com.huawei.www.bme.cbsinterface.masterslavemgrtmgr.MasterSlavemgrtMgr;
import com.huawei.www.bme.cbsinterface.masterslavemgrtmgr.MasterSlavemgrtMgrServiceLocator;
import com.huawei.www.bme.cbsinterface.usergroupmgrt.CreateUserGroupRequest;
import com.huawei.www.bme.cbsinterface.usergroupmgrt.CreateUserGroupRequestGroupInfo;
import com.huawei.www.bme.cbsinterface.usergroupmgrt.CreateUserGroupRequestOptionalOffer;
import com.huawei.www.bme.cbsinterface.usergroupmgrt.CreateUserGroupRequestOptionalOfferProduct;
import com.huawei.www.bme.cbsinterface.usergroupmgrt.DeleteUserGroupRequest;
import com.huawei.www.bme.cbsinterface.usergroupmgrt.DeleteUserGroupRequestGroupInfo;
import com.huawei.www.bme.cbsinterface.usergroupmgrt.ManageUserGroupMemberRequest;
import com.huawei.www.bme.cbsinterface.usergroupmgrt.ManageUserGroupMemberRequestGroupInfo;
import com.huawei.www.bme.cbsinterface.usergroupmgrt.ManageUserGroupMemberRequestMemberInfo;
import com.huawei.www.bme.cbsinterface.usergroupmgrt.QueryUserGroupRequest;
import com.huawei.www.bme.cbsinterface.usergroupmgrtmgr.*;
import com.huawei.www.bme.cbsinterface.delsubscriber.*;
import com.huawei.www.bme.cbsinterface.delsubscribermgr.*;
import com.huawei.www.bme.cbsinterface.negbalance.*;
import com.huawei.www.bme.cbsinterface.negbalancemgr.*;
import com.huawei.www.bme.cbsinterface.mansubquota.ManSubQuotaRequest;
import com.huawei.www.bme.cbsinterface.mansubquota.ManSubQuotaRequestNotifyNode;
import com.huawei.www.bme.cbsinterface.mansubquota.QuerySubQuotaRequest;
import com.huawei.www.bme.cbsinterface.mansubquotamgr.*;


public class OCSObjprov3 {

	public Submgrt service_Submgrt;
	public Notify service_Notify;
	public AdjustAccountMgr service_AdjustAccountMgr;
	public SubscribeMgr service_SubscribeMgr;
	public Acctcredit service_Acctcredit;
	public CallScreenMgr service_CallScreenMgr;
	public FamilyNoMgr service_FamilyNoMgr;
	public DualNumberMgr service_DualNumberMgr;
	public UserGroupMgrt service_UserGroupMgrt;
	public CUGEnhance service_CUGEnhance;
	public CUG service_CUG;
	public QueryMgr service_QueryMgr;
	public MasterSlavemgrtMgr service_MasterSlavemgrtMgr;
	public CABMgr service_CABMgr;
	public NewSubscriberMgr service_NewSubscriberMgr;
	public ChangePrimaryOfferMgr service_ChangePrimaryOfferMgr;
	public ProductOfferMgr service_Productoffermgr;
	public CashRechargeMgr service_CashRechargeMgr;
	public DelSubscriberMgr   service_Delsubscribermgr;
	public NegBalanceMgr service_NegBalanceMgr;
	public Mansubquota service_masubQuotaMgr;
	private String ocs3_testing_numbers;
	private String numbers;
	private String po2mp;
	private String oo2ap;
	private String mp2tap;
	private String mp2faf;
	private String mp2fafold;
	private String mp2sog;
	private String mp2sogold;
	private ResourceBundle RB;
	
	public OCSObjprov3(){
		
		try
		{
		
			service_Submgrt = new SubmgrtMgrServiceLocator().getSubmgrtServicePort(new URL("http://10.56.180.75:7781/services/SubmgrtMgrService?wsdl")); //10.56.176.167:7782    10.48.176.20:7781 10.56.180.75:7781
			service_Notify  = new NotifyMgrServiceLocator().getNotifyServicePort(new URL("http://10.56.180.75:7781/services/NotifyMgrService?wsdl"));
			service_AdjustAccountMgr = new AdjustAccountMgrServiceLocator().getAdjustAccountMgrServicePort(new URL("http://10.56.176.167:7782/services/AdjustAccountMgrService?wsdl"));
			service_SubscribeMgr = new SubscribeMgrServiceLocator().getSubscribeMgrServicePort(new URL("http://10.56.180.75:7781/services/SubscribeMgrService?wsdl"));
			service_Acctcredit  = new AcctcreditMgrServiceLocator().getAcctcreditServicePort(new URL("http://10.56.180.75:7781/services/AcctcreditMgrService?wsdl"));
			service_CallScreenMgr = new  CallScreenMgrServiceLocator().getCallScreenMgrServicePort(new URL("http://10.56.180.75:7781/services/CallScreenMgrService?wsdl"));
			service_FamilyNoMgr = new FamilyNoMgrServiceLocator().getFamilyNoMgrServicePort(new URL("http://10.56.180.75:7781/services/FamilyNoMgrService?wsdl"));
			service_DualNumberMgr = new DualNumberMgrServiceLocator().getDualNumberMgrServicePort(new URL("http://10.56.180.75:7781/services/DualNumberMgrService?wsdl"));
			service_UserGroupMgrt = new UserGroupMgrServiceLocator().getUserGroupMgrServicePort(new URL("http://10.56.180.75:7781/services/UserGroupMgrService?wsdl"));
			service_CUGEnhance = new CUGEnhanceMgrServiceLocator().getCUGEnhanceServicePort(new URL("http://10.56.180.75:7781/services/CUGEnhanceMgrService?wsdl"));
			service_CUG = new CUGMgrServiceLocator().getCUGServicePort(new URL("http://10.56.180.75:7781/services/CUGMgrService?wsdl"));
			service_QueryMgr = new QueryMgrServiceLocator().getQueryMgrServicePort(new URL("http://10.56.180.75:7781/services/QueryMgrService?wsdl"));
			service_MasterSlavemgrtMgr = new MasterSlavemgrtMgrServiceLocator().getMasterSlavemgrtMgrServicePort(new URL("http://10.56.180.75:7781/services/MasterSlavemgrtMgrService?wsdl"));
			service_CABMgr = new CABMgrServiceLocator().getCABMgrServicePort(new URL("http://10.56.180.75:7781/services/CABMgrService?wsdl"));
			service_NewSubscriberMgr = new NewSubscriberMgrServiceLocator().getNewSubscriberMgrServicePort(new URL("http://10.56.180.75:7781/services/NewSubscriberMgrService?wsdl"));
			service_ChangePrimaryOfferMgr = new ChangePrimaryOfferMgrServiceLocator().getChangePrimaryOfferMgrServicePort(new URL("http://10.56.180.75:7781/services/ChangePrimaryOfferMgrService?wsdl"));
			service_Productoffermgr = new ProductOfferMgrServiceLocator().getProductOfferMgrServicePort(new URL("http://10.56.180.75:7781/services/ProductOfferMgrService?wsdl"));
			service_CashRechargeMgr	= new CashRechargeMgrServiceLocator().getCashRechargeMgrServicePort(new URL("http://10.56.180.75:7781/services/CashRechargeMgrService?wsdl"));
			service_Delsubscribermgr = new DelSubscriberMgrServiceLocator().getDelSubscriberMgrServicePort(new URL("http://10.56.180.75:7781/services/DelSubscriberMgrService?wsdl"));
			service_NegBalanceMgr = new NegBalanceMgrServiceLocator().getNegBalanceMgrServicePort(new URL("http://10.56.180.75:7781/services/NegBalanceMgrService?wsdl"));
			service_masubQuotaMgr = new ManSubQuotaMgrServiceLocator().getmansubquotaServicePort(new URL("http://10.56.180.75:7781/services/ManSubQuotaMgrService?wsdl"));
	 		RB= ResourceBundle.getBundle("ocs3_3_init");
	 		ocs3_testing_numbers=RB.getString("ocs3_testing_numbers");
	 		System.out.println("ocs3_testing_numbers ="+ocs3_testing_numbers);
	 		numbers=RB.getString("numbers");
	 		System.out.println("number levels ="+numbers);
	 		po2mp=RB.getString("po2mp");
	 		System.out.println("po2mp ="+po2mp);
	 		oo2ap=RB.getString("oo2ap");
	 		System.out.println("oo2ap ="+oo2ap);
	 		mp2tap=RB.getString("mp2tap");
	 		System.out.println("mp2tap ="+mp2tap);
	 		mp2faf=RB.getString("mp2faf");
	 		System.out.println("mp2faf ="+mp2faf);
			mp2fafold=RB.getString("mp2fafold");
			System.out.println("mp2fafold ="+mp2fafold);
			mp2sog=RB.getString("mp2sog");
			System.out.println("mp2sog ="+mp2sog);
			mp2sogold=RB.getString("mp2sogold");
			System.out.println("mp2sogold ="+mp2sogold);
		
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}

	public RequestHeader getHeader(String command) throws Exception
	{
		RequestHeader reqh= new RequestHeader();
		Date a =new Date();
		long seq = a.getTime();
		String seqstr ="WS_OCS3.3"+seq;
		RequestHeaderRequestType rType =RequestHeaderRequestType.Event;
		reqh.setCommandId(command);
		reqh.setVersion("1");
		reqh.setTransactionId("Null");
		reqh.setSequenceId("1");
		reqh.setRequestType(rType );
		reqh.setSerialNo(seqstr);
		SessionEntityType sese = new SessionEntityType("SME","","ABACUS");
		reqh.setSessionEntity(sese);
		reqh.setRemark("OCSMED_3.3");
		reqh.setTenantId(new BigInteger("100009400"));
		return reqh;
	}
	public String accountAdjustment(String msisdn,String amnt ,String accCode ,String dd) throws Exception
	{
		
		RequestHeader reqh= getHeader("AdjustAccount");
		AdjustAccountRequest adjreq = new AdjustAccountRequest();
		adjreq.setSubscriberNo(msisdn);
		adjreq.setOperateType(2);
		adjreq.setReason(100);
		adjreq.setSPCode("SME");
		ModifyAcctFeeType[] mafarr = new  ModifyAcctFeeType[1];
		ModifyAcctFeeType maf1 = new  ModifyAcctFeeType();
		maf1.setAccountType(accCode);
		maf1.setCurrAcctChgAmt(Long.parseLong(amnt));
		
		maf1.setExpireTime(addDaysToDate(dd,1));
		mafarr[0] =maf1;
		
		adjreq.setModifyAcctFeeList(mafarr);
		AdjustAccountRequestMsg req4 = new AdjustAccountRequestMsg();
		req4.setRequestHeader(reqh);
		req4.setAdjustAccountRequest(adjreq);
		AdjustAccountResultMsg test = service_AdjustAccountMgr.adjustAccount(req4);
		
		System.out.println(msisdn+"|"+test.getResultHeader().getResultCode()+"|"+test.getResultHeader().getResultDesc());
		
		if(test.getResultHeader().getResultCode().equals("405000000"))
		{
			return "OK";
		}
		else
		{
			return test.getResultHeader().getResultDesc();
		}		
			//return "NOK";
		
	}
	public String accountAdjustmentWithReason(String msisdn,String amnt,int reason,String accType) throws Exception
	{
		
		RequestHeader reqh= getHeader("AdjustAccount");
		AdjustAccountRequest adjreq = new AdjustAccountRequest();
		adjreq.setSubscriberNo(msisdn);
		adjreq.setOperateType(2);
		adjreq.setReason(reason);
		adjreq.setSPCode("OBJ");
		ModifyAcctFeeType[] mafarr = new  ModifyAcctFeeType[1];
		ModifyAcctFeeType maf1 = new  ModifyAcctFeeType();
		maf1.setCurrAcctChgAmt(Long.parseLong(amnt));
		maf1.setAccountType(accType);
		mafarr[0] =maf1;
		
		adjreq.setModifyAcctFeeList(mafarr);
		AdjustAccountRequestMsg req4 = new AdjustAccountRequestMsg();
		req4.setRequestHeader(reqh);
		req4.setAdjustAccountRequest(adjreq);
		AdjustAccountResultMsg test = service_AdjustAccountMgr.adjustAccount(req4);
		
		System.out.println(msisdn+"|"+test.getResultHeader().getResultCode()+"|"+test.getResultHeader().getResultDesc());
		
		if(test.getResultHeader().getResultCode().equals("405000000"))
		{
			return "0|0|0|0|0|0|0";
		}
		else
		{
			return "1|1|1|1|1|1|"+test.getResultHeader().getResultDesc();
		}		
			//return "NOK";
		
	}
	public String getBalance(String msisdn ,String acc_type ,int bal) throws Exception
	{
		System.out.println("Calling get balance:" + msisdn);
		RequestHeader reqh =getHeader("QueryBalance");
		QueryBalanceRequest sub = new QueryBalanceRequest();
		sub.setSubscriberNo(msisdn);
		QueryBalanceRequestMsg req = new QueryBalanceRequestMsg();
		req.setQueryBalanceRequest(sub);
		req.setRequestHeader(reqh);
		QueryBalanceResultMsg test = new QueryBalanceResultMsg();
		try
		{
	   test = service_QueryMgr.queryBalance(req);
		}
		catch(Exception e)
		{
			System.out.println("Error in side ocsobj "+e);
		}
		
	//	System.out.println(test.getResultHeader().getResultCode());  
		if(test.getResultHeader().getResultCode().equals("405000000"))
		{
		int k=0;
		while( k < test.getQueryBalanceResult().getBalanceRecord().length)
		{
		if(test.getQueryBalanceResult().getBalanceRecord(k).getAccountType().equals(acc_type))
		{
			  //System.out.println(test.getQueryBalanceResult().getBalanceRecord(k).getAmount()+"==="+bal);  
				if(bal <= (int)(test.getQueryBalanceResult().getBalanceRecord(k).getAmount()))
				{
					return "OK";
				}
				else
				{
					return "Balance Not Suffcient";
				}
					
		 
		}
		k++;
		}			
				
		}
		else
		{
			return test.getResultHeader().getResultCode();
		}
			return test.getResultHeader().getResultCode();
		
	}
	public String getAccBalancefromAccType(String msisdn ,String acc_type) throws Exception
	{
		RequestHeader reqh =getHeader("QueryBalance");
		QueryBalanceRequest sub = new QueryBalanceRequest();
		sub.setSubscriberNo(msisdn);
		QueryBalanceRequestMsg req = new QueryBalanceRequestMsg();
		req.setQueryBalanceRequest(sub);
		req.setRequestHeader(reqh);
		QueryBalanceResultMsg test = new QueryBalanceResultMsg();
		try
		{
	   test = service_QueryMgr.queryBalance(req);
		}
		catch(Exception e)
		{
			System.out.println("Error in side ocsobj "+e);
		}
		String retstr="0|";	
		if(test.getResultHeader().getResultCode().equals("405000000"))
		{ 
		
		int k=0;
		while( k < test.getQueryBalanceResult().getBalanceRecord().length)
		{
		if(test.getQueryBalanceResult().getBalanceRecord(k).getAccountType().equals(acc_type))
		{
			retstr=retstr+test.getQueryBalanceResult().getBalanceRecord(k).getAmount()+","+test.getQueryBalanceResult().getBalanceRecord(k).getExpireTime()+"000000|";	
		}
		
		k++;
		}			
		
				
		}
		else
		{
			retstr="1|"+test.getResultHeader().getResultDesc();
		}
		return retstr;
		
	}
	
	public String getAccBalancefromAccTypearray(String msisdn ,String [] acc_type ) throws Exception
	{
		RequestHeader reqh =getHeader("QueryBalance");
		QueryBalanceRequest sub = new QueryBalanceRequest();
		sub.setSubscriberNo(msisdn);
		QueryBalanceRequestMsg req = new QueryBalanceRequestMsg();
		req.setQueryBalanceRequest(sub);
		req.setRequestHeader(reqh);
		QueryBalanceResultMsg test = new QueryBalanceResultMsg();
		try
		{
	   test = service_QueryMgr.queryBalance(req);
		}
		catch(Exception e)
		{
			System.out.println("Error in side ocsobj "+e);
		}
		String retstr="0|";	
		if(test.getResultHeader().getResultCode().equals("405000000"))
		{ 
		
		int k=0;
		while( k < test.getQueryBalanceResult().getBalanceRecord().length)
		{
		
		int w = 0;
		while(w < acc_type.length)
		{
			System.out.println("INSIDE OBJ ALL : "+test.getQueryBalanceResult().getBalanceRecord(k).getAccountType());
		if(test.getQueryBalanceResult().getBalanceRecord(k).getAccountType().equals(acc_type[w]))
		{
			System.out.println("INSIDE OBJ : "+acc_type[w]);
			retstr=retstr+acc_type[w]+","+test.getQueryBalanceResult().getBalanceRecord(k).getAmount()+","+test.getQueryBalanceResult().getBalanceRecord(k).getExpireTime()+"000000|";	
		}
		w++;
		
		}
		
		k++;
		}			
		
				
		}
		else
		{
			retstr="1|"+test.getResultHeader().getResultDesc();
		}
		return retstr;
		
	}
	
	
	
	
	
	public String accAdjustReq(String msisdn,String amount,String reason,String app,String sysid) throws Exception
	{
    System.out.println("accAdjustReq Strating:");
    System.out.println(amount);
		Date a = new Date();
		long b = a.getTime();
		Random generator = new Random(19580427);
		int randnum = generator.nextInt(10);
		int amountint =(int)(Float.parseFloat(amount)*10000);
		
		//System.out.println(amountint);
		
		String dbflag="0";
		if(amountint<0)
		{
			amountint=-1*amountint;
			dbflag="1";
		}

		String url1 = "http://10.56.176.168:5581/servlet/RequestS2?request="+sysid+"|acc_adj_request|SP"+b+""+randnum+"|"+msisdn+"|"+dbflag+"|"+amountint+"|"+reason+"|"+app;
	  //System.out.println("Req:"+url1);
		URL corba3 = new URL(url1);
		BufferedReader in3 = new BufferedReader(new InputStreamReader(corba3.openStream()));
		String inputLine3;
		inputLine3 = in3.readLine();
		in3.close();
		return inputLine3;
	}
	
	
	
	public String accTopupReq(String msisdn,String amount) throws Exception
	{

		Date a = new Date();
		long b = a.getTime();
		Random generator = new Random(19580427);
		int randnum = generator.nextInt(10);
		
		String url1 = "http://10.56.176.168:5581/servlet/RequestS2?request=41|acc_topup_request|SP"+b+""+randnum+"|"+msisdn+"|"+amount+"|747|INTCARD";
		//System.out.println("Req:"+url1);
		URL corba3 = new URL(url1);
		BufferedReader in3 = new BufferedReader(new InputStreamReader(corba3.openStream()));
		String inputLine3;
		inputLine3 = in3.readLine();
		in3.close();
		return inputLine3;

	}
	public String subdesubApendantProduct(String msisdn, String prid,String effDate,String extDate, boolean effimmediate ,int opr) throws Exception
	{
		RequestHeader reqh= getHeader("ChangeOptionalOffer");
		ChangeOptionalOfferRequest sub = new ChangeOptionalOfferRequest();
		sub.setSubscriberNo(msisdn);
		ChangeOptionalOfferRequestOptionalOffer[] opffer =  new ChangeOptionalOfferRequestOptionalOffer[1];
		
		
		ChangeOptionalOfferRequestOptionalOffer opffer_obj = new ChangeOptionalOfferRequestOptionalOffer();	
		opffer_obj.setId(Integer.parseInt(prid));
		opffer_obj.setOperationType(opr);	
		EffectMode em = new EffectMode();
		if(effimmediate)
		{
		em.setMode(EffectModeMode.value1);
		em.setExpireDate(extDate);
		}
		else
		{
		em.setMode(EffectModeMode.value4);	
		em.setEffectiveDate(effDate);
		em.setExpireDate(extDate);
		}
		opffer_obj.setValidMode(em);
		opffer[0] = opffer_obj;
		
		
		sub.setOptionalOffer(opffer);
		ChangeOptionalOfferRequestMsg req = new ChangeOptionalOfferRequestMsg();
		req.setRequestHeader(reqh);
		req.setChangeOptionalOfferRequest(sub);
		ChangeOptionalOfferResultMsg test = service_SubscribeMgr.changeOptionalOffer(req);
	
		String result2 = test.getResultHeader().getResultDesc();
			
		return result2;	
		
	}
	public String subscribeApendantProductWithExpEffectTime(String msisdn, String prid,String effDate,String extDate, boolean effimmediate) throws Exception
	{
		return  subdesubApendantProduct(msisdn,prid,effDate,extDate,effimmediate,1);
	}
	public String unSubscribeApendantProduct2(String msisdn, String prid) throws Exception
	{
		return subdesubApendantProduct(msisdn,prid,"","",true,2);
	}
	public String addRemApendantProductWithExpDays(String msisdn, String prid,int opt, int expDays) throws Exception
	{
		Date a2 = new Date();
		DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String current_d = sdf.format(a2);
		String expd = addDaysToDate(current_d,expDays);
		System.out.println(current_d+" "+expd);
		return subdesubApendantProduct(msisdn,prid,current_d,expd,true,opt);
					
	}
	public String addRemApendantProduct2(String msisdn, String prid,int opt) throws Exception
	{
		return subdesubApendantProduct(msisdn,prid,"","",true,opt);
	}
	public String getMainProductName(String msisdn) throws Exception
	{
		RequestHeader reqh =getHeader("QueryCustomer");
		com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest subq = new com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest();
		subq.setSubscriberNo(msisdn);
		subq.setQueryType(1);
		
		com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg req = new com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg();
		req.setQueryCustomerRequest(subq);
		req.setRequestHeader(reqh);
		QueryCustomerResultMsg test =  service_QueryMgr.queryCustomer(req);
					
		if(test.getResultHeader().getResultDesc().equals("Operation successfully."))
		{
		
		int k=0;
		int primOfferID=0;
		
		while(k < test.getQueryCustomerResult().getSubscriber().length)
		{
		  if(test.getQueryCustomerResult().getSubscriber(k).getSubscriberInfo().getPrimaryOfferId() > 0)
		  {
		   	primOfferID	=test.getQueryCustomerResult().getSubscriber(k).getSubscriberInfo().getPrimaryOfferId();	
		  	break;
		  }
		  k++;
		}
		
		RequestHeader head_offer= getHeader("QueryOfferInfoRequest");
		QueryOfferInfoRequest sub = new QueryOfferInfoRequest();
		sub.setId(primOfferID);
		QueryOfferInfoRequestMsg  req_offer = new QueryOfferInfoRequestMsg();
		req_offer.setQueryOfferInfoRequest(sub);
		req_offer.setRequestHeader(head_offer);
		QueryOfferInfoResultMsg  test_offer = service_Productoffermgr.queryOfferInfo(req_offer);
		
		if(test_offer.getResultHeader().getResultDesc().equals("Operation successfully."))
		{
			
		return "0,"+test_offer.getQueryOfferInfoResult().getOfferInfo().getName();
			
		}
		else
		{
		     return "1,QueryProductInfo:"+test.getResultHeader().getResultDesc();	
		}
		
		
		}
		else
		{
			return "1,QueryBasicInfo:"+test.getResultHeader().getResultDesc();	
	
		}
		
		
		
		
	}
	public String AddRemCugGroup(String member, String GroupId,int operationType) throws Exception
	{
		RequestHeader reqh= getHeader("ManGroupMember");
		com.huawei.www.bme.cbsinterface.cug.ManGroupMemberRequest qUserG = new com.huawei.www.bme.cbsinterface.cug.ManGroupMemberRequest();
		if(GroupId!=null)
		qUserG.setUserGroupID(Integer.parseInt(GroupId));
		qUserG.setSubscriberNo(member);
		qUserG.setOperationType(operationType);
		ManGroupMemberRequestMsg req4 = new ManGroupMemberRequestMsg();
		req4.setRequestHeader(reqh);
		req4.setManGroupMemberRequest(qUserG);
		ManGroupMemberResultMsg test = service_CUG.manGroupMember(req4);
		return test.getResultHeader().getResultDesc(); 
		
	}
	public String EditCugGroup(String  msisdn ,String bnum ,Integer opt) throws Exception
	{
		RequestHeader reqh =getHeader("ManCUGMemberCallList");
        ManCUGMemberCallListRequest sub = new ManCUGMemberCallListRequest();
        sub.setSubscriberNo(msisdn);
        ManCUGMemberCallListRequestCallNumberListCallNumberInfo[] callnoInfo = new ManCUGMemberCallListRequestCallNumberListCallNumberInfo[1];
        ManCUGMemberCallListRequestCallNumberListCallNumberInfo  callnoInfo_obj = new ManCUGMemberCallListRequestCallNumberListCallNumberInfo();
        callnoInfo_obj.setCallNumber(bnum);
        callnoInfo_obj.setNumberType(1);
        callnoInfo_obj.setOperationType(opt);
        callnoInfo[0] = callnoInfo_obj;
        sub.setCallNumberList(callnoInfo);
        ManCUGMemberCallListRequestMsg req = new ManCUGMemberCallListRequestMsg();
        req.setRequestHeader(reqh);
        req.setManCUGMemberCallListRequest(sub);
        ManCUGMemberCallListResultMsg test = service_CUGEnhance.manCUGMemberCallList(req);
      
		return test.getResultHeader().getResultDesc();
      
		
	}
	public boolean firstActivation(String msisdn) throws Exception
	{
		RequestHeader reqh =getHeader("ActiveFirst");
		ActiveFirstRequest sub = new ActiveFirstRequest();
		sub.setSubscriberNo(msisdn);
		ActiveFirstRequestMsg req = new ActiveFirstRequestMsg();
		req.setActiveFirstRequest(sub);
		req.setRequestHeader(reqh);
		ActiveFirstResultMsg test = service_CashRechargeMgr.activeFirst(req);
		if(test.getResultHeader().getResultDesc().equals("Operation succeeded."))
		{
			return true;	
		}
		else
		{
			return false;	
		}
	
		
	}
	public String getAppProduct(String msisdn, String pridstr) throws Exception
	{
		
		RequestHeader reqh =getHeader("QueryCustomer");
		com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest subq = new com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest();
		subq.setSubscriberNo(msisdn);
		subq.setQueryType(1);
		
		com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg req = new com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg();
		req.setQueryCustomerRequest(subq);
		req.setRequestHeader(reqh);
		QueryCustomerResultMsg test =  service_QueryMgr.queryCustomer(req);
			
		
		String rslt="NO";
		if(test.getQueryCustomerResult().getCustomer().getOffer().length  > 0)
		{
			for(int i=0;i < test.getQueryCustomerResult().getCustomer().getOffer().length;i++)
			{
				String t_chk =String.valueOf(test.getQueryCustomerResult().getCustomer().getOffer(i).getOfferId());
				String [] tempArr=null;
				tempArr=pridstr.split(",");
				for(int ki=0;ki<tempArr.length;ki++)
				{
					System.out.println(t_chk+"|"+tempArr[ki]);
					if(tempArr[ki].equals(t_chk))
					{
						
						rslt=t_chk; 
						break;
					}
				}
				if(!rslt.equals("NO"))
				{
					break;
				}
				
			}
			return rslt;
		}
		else
		{
			return rslt;
		}
	}
	public int getAppProductStatus(String msisdn,String plist) throws Exception
	{
		RequestHeader reqh =getHeader("QueryCustomer");
		com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest subq = new com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest();
		subq.setSubscriberNo(msisdn);
		subq.setQueryType(1);
		com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg req = new com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg();
		req.setQueryCustomerRequest(subq);
		req.setRequestHeader(reqh);
		QueryCustomerResultMsg test =  service_QueryMgr.queryCustomer(req);
		
		if(test.getQueryCustomerResult().getCustomer().getOffer().length  > 0)
		{
			for(int i=0;i < test.getQueryCustomerResult().getCustomer().getOffer().length;i++)
			{
			  if(plist.equalsIgnoreCase(String.valueOf(test.getQueryCustomerResult().getCustomer().getOffer(i).getOfferId() )))
			  {
				  return 0;
			  }
			}
			
			return -1;
			
		}
		else
		{
			return -1;
		}
	}
	public String SogQuary(String pnum) throws Exception
	{
		RequestHeader reqh =getHeader("QueryUserGroup");
		QueryUserGroupRequest sub = new QueryUserGroupRequest();
		sub.setSubscriberNo(pnum);
		QueryUserGroupRequestMsg req = new QueryUserGroupRequestMsg();
		req.setQueryUserGroupRequest(sub);
		req.setRequestHeader(reqh);
		com.huawei.www.bme.cbsinterface.usergroupmgrtmgr.QueryUserGroupResultMsg test = service_UserGroupMgrt.queryUserGroup(req);
		System.out.println(test.getResultHeader().getResultDesc());		
		int j=0;
		String supNo="";
		String gname="";
		String prodID="";
		String primaryNum="";
		if(test.getQueryUserGroupResult()!=null)
		{
			while(j < test.getQueryUserGroupResult().length)
			{
				System.out.println("User Group type: "+test.getQueryUserGroupResult()[j].getGroupBasicInfo().getGroupType());
				if(test.getQueryUserGroupResult()[j].getGroupBasicInfo().getGroupType() == 100)
				{
					gname = test.getQueryUserGroupResult()[j].getGroupBasicInfo().getGroupName();
					
					if(test.getQueryUserGroupResult()[j].getMemberInfo() != null)
					{
						int k =0;
						while(k < test.getQueryUserGroupResult()[j].getMemberInfo().length)
							{
							if(test.getQueryUserGroupResult()[j].getMemberInfo(k).getMemberType() ==100)
							{
								primaryNum= test.getQueryUserGroupResult()[j].getMemberInfo(k).getSubscriberNo();
								System.out.println("MEM:"+primaryNum+"|"+test.getQueryUserGroupResult()[j].getMemberInfo(k).getMemberType()+"|");
								//break;
							}
							else
							{
								supNo = test.getQueryUserGroupResult()[j].getMemberInfo(k).getSubscriberNo();
							}
							k++;
							}
					}
						
					if(test.getQueryUserGroupResult()[j].getOfferOrderInfo() != null)
					{
						prodID =String.valueOf(test.getQueryUserGroupResult()[j].getOfferOrderInfo(0).getOfferId());
					}
					
					
					
					
					return  1+","+gname+","+primaryNum+","+supNo+","+prodID;
						
				
				}
			}
		}
		return "0";
		
		
		
	}
	public String getSubsInfo(String msisdn) throws Exception
	{
				RequestHeader reqh =getHeader("QueryCustomer");
        com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest sub = new com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest();
        sub.setSubscriberNo(msisdn);
        sub.setQueryType(1);
        com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg    req = new com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg();
        req.setQueryCustomerRequest(sub);
        req.setRequestHeader(reqh);
        QueryCustomerResultMsg test = service_QueryMgr.queryCustomer(req);
        
        if(test.getResultHeader().getResultCode().equals("405000000"))
        {
	        String status="";
					int st = test.getQueryCustomerResult().getSubscriber(0).getSubscriberState().getLifeCycleState();
					
					if(st == 0) status="VALIDE";
					else if(st == 1) status="ACTIF";
					else if(st == 2) status="INACT";
					else if(st == 3) status="DEACT";
					else if(st == 4) status="DEACT";
					else status="VALIDE";
					
					String mpdt =String.valueOf(test.getQueryCustomerResult().getSubscriber(0).getSubscriberInfo().getPrimaryOfferId());
					mpdt = getProductName(mpdt);
					String paidmd = String.valueOf(test.getQueryCustomerResult().getSubscriber(0).getSubscriberInfo().getPaidMode());
					String imsi = test.getQueryCustomerResult().getSubscriber(0).getSubscriberInfo().getIMSI();
					String mpdtId = String.valueOf(test.getQueryCustomerResult().getSubscriber(0).getSubscriberInfo().getPrimaryOfferId());
					String dpfi = String.valueOf(test.getQueryCustomerResult().getSubscriber(0).getSubscriberState().getDPFlag());
					
					int k=0;
					String sogf = "0";
					while(k < test.getQueryCustomerResult().getSubscriber(0).getService().length)
					{
						if(test.getQueryCustomerResult().getSubscriber(0).getService(k).getId() == 26)
						{
							try
							{
								sogf = SogQuary(msisdn);
								String [] sog_arr = sogf.split(",");	
								sogf = sog_arr[0];
							}
							catch(Exception e)
							{
								sogf = "0";
							}
						}
						k++;
					}
				
			    
					int k1=0;
					String balance="";
					while(k1 < test.getQueryCustomerResult().getAccount().length)
					{			
						int j=0;
						
						while( j < test.getQueryCustomerResult().getAccount(k1).getBalanceRecordList().getBalance().length)
						{
							if(test.getQueryCustomerResult().getAccount(k1).getBalanceRecordList().getBalance(j).getAccountType().equals("2000"))
							{
								balance=String.valueOf(test.getQueryCustomerResult().getAccount(k1).getBalanceRecordList().getBalance(j).getAmount());
							}
						 	j++;
						}
						k1++;
						
					}
					//////////////////////////////////////////
					int k2=0;
					String tariffMode="0";//0-PM,1-PS
					String sogTariff="0";
					String fafTariff="0";
					int mainProductId = getMainprodId(mpdtId);
					int pmTariffProd = getTariffOfferID(mainProductId,0);
					int psTariffProd = getTariffOfferID(mainProductId,1);
					
					int sogoldps=getSogOldOfferID(mainProductId,1);
					int sogoldpm=getSogOldOfferID(mainProductId,0);
					int sogps=getSogOfferID(mainProductId,1);
					int sogpm=getSogOfferID(mainProductId,0);
					
					int fnoldps=getFafOldOfferID(mainProductId,1);
					int fnoldpm=getFafOldOfferID(mainProductId,0);
					int fnps=getFafOfferID(mainProductId,1);
					int fnpm=getFafOfferID(mainProductId,0);
					
					boolean isTariffFound=false;
					//$ProdList =  $query->QueryCustomerResult->Subscriber->ProductList;
					 com.huawei.www.bme.cbsinterface.query.ProductOrderInfo[] pdtList=test.getQueryCustomerResult().getSubscriber(0).getProductList();
					while(k2 < pdtList.length)
					{
						if(pdtList[k2].getOfferId()==pmTariffProd && !isTariffFound)
						{
							tariffMode="0";isTariffFound=true;
						}
						else if(pdtList[k2].getOfferId()==psTariffProd && !isTariffFound)
						{
							tariffMode="1";isTariffFound=true;
						}
						else if (pdtList[k2].getOfferId()==sogoldps||pdtList[k2].getOfferId()==sogoldpm)
						{
							sogTariff="1";//OLD SOG
						}
						else if (pdtList[k2].getOfferId()==sogps||pdtList[k2].getOfferId()==sogpm)
						{
							sogTariff="2";//OLD SOG
						}
						else if (pdtList[k2].getOfferId()==fnoldps||pdtList[k2].getOfferId()==fnoldpm)
						{
							fafTariff="1";//OLD SOG
						}
						else if (pdtList[k2].getOfferId()==fnps||pdtList[k2].getOfferId()==fnpm)
						{
							fafTariff="2";//OLD SOG
						}
						k2++;
					}
				////////////////////////////
				return "0,"+mpdt+","+status+","+balance+",0,"+sogf+","+dpfi+","+paidmd+","+imsi+","+mpdtId+","+tariffMode+","+fafTariff+","+sogTariff;	
				
    }
    else
    {
    	return  "1,"+test.getResultHeader().getResultDesc();
		
    }
    
		
	}
	public String SogAddRemWithPID(String msisdn,int action,String pid) throws Exception
	{
		if(action == 1)
		{
		RequestHeader reqh = getHeader("CreateUserGroup");
		CreateUserGroupRequest sub = new CreateUserGroupRequest();
		sub.setOwnerSubscriberNo(msisdn);
		CreateUserGroupRequestGroupInfo Ginfor = new CreateUserGroupRequestGroupInfo();
		Ginfor.setGroupName(msisdn);
		Ginfor.setGroupType(100);
		sub.setGroupInfo(Ginfor);
	
		CreateUserGroupRequestOptionalOffer[] opffer =  new CreateUserGroupRequestOptionalOffer[1];		
		CreateUserGroupRequestOptionalOffer opffer_obj = new CreateUserGroupRequestOptionalOffer();	
		opffer_obj.setId(Integer.parseInt(pid));
		EffectMode em = new EffectMode();
		em.setMode(EffectModeMode.value1);
		opffer_obj.setValidMode(em);
		
		CreateUserGroupRequestOptionalOfferProduct[] offer_prod = new CreateUserGroupRequestOptionalOfferProduct[1];
		CreateUserGroupRequestOptionalOfferProduct  offer_prod_obj = new CreateUserGroupRequestOptionalOfferProduct();
		int prod=getAppProdId(pid) ; //  Need to maintain the relevent  product ID for offer ids
		offer_prod_obj.setId(prod);
		offer_prod[0] = offer_prod_obj;
		opffer_obj.setProduct(offer_prod);
				
		opffer[0] = opffer_obj;
			
		sub.setOptionalOffer(opffer);
		CreateUserGroupRequestMsg req = new CreateUserGroupRequestMsg();
		req.setRequestHeader(reqh);
		req.setCreateUserGroupRequest(sub);
		CreateUserGroupResultMsg test = service_UserGroupMgrt.createUserGroup(req);
				
		return test.getResultHeader().getResultDesc();
		}
		else
		{
		RequestHeader reqh = getHeader("DeleteUserGroup");
		DeleteUserGroupRequest sub = new DeleteUserGroupRequest();
		sub.setOwnerSubscriberNo(msisdn);
		DeleteUserGroupRequestGroupInfo delu = new DeleteUserGroupRequestGroupInfo();
		delu.setGroupType(100);
		sub.setGroupInfo(delu);
		DeleteUserGroupRequestMsg req = new DeleteUserGroupRequestMsg();
		req.setRequestHeader(reqh);
		req.setDeleteUserGroupRequest(sub);
		DeleteUserGroupResultMsg test=service_UserGroupMgrt.deleteUserGroup(req);
	
		return test.getResultHeader().getResultDesc();
        
		}
				
	}
	public boolean removeSubscriber(String msisdn) throws Exception
	{
		RequestHeader reqh = getHeader("DelSubscriber");
		DelSubscriberRequest sub = new DelSubscriberRequest();
		sub.setSubscriberNo(msisdn);
		DelSubscriberRequestMsg req= new  DelSubscriberRequestMsg();
		req.setDelSubscriberRequest(sub);
		req.setRequestHeader(reqh);
		DelSubscriberResultMsg test = service_Delsubscribermgr.delSubscriber(req);
				
		if((test.getResultHeader().getResultCode().equals("405000000"))) return true;
		else return false;
		
		
	}
	public boolean subscribeApendantProduct(String msisdn, String prid) throws Exception
	{
		RequestHeader reqh= getHeader("ChangeOptionalOffer");
		ChangeOptionalOfferRequest sub = new ChangeOptionalOfferRequest();
		sub.setSubscriberNo(msisdn);
		ChangeOptionalOfferRequestOptionalOffer[] opffer =  new ChangeOptionalOfferRequestOptionalOffer[1];
		
		
		ChangeOptionalOfferRequestOptionalOffer opffer_obj = new ChangeOptionalOfferRequestOptionalOffer();	
		opffer_obj.setId(Integer.parseInt(prid));
		opffer_obj.setOperationType(1);	
		EffectMode em = new EffectMode();
		em.setMode(EffectModeMode.value1);
		opffer_obj.setValidMode(em);
		opffer[0] = opffer_obj;
				
		sub.setOptionalOffer(opffer);
		ChangeOptionalOfferRequestMsg req = new ChangeOptionalOfferRequestMsg();
		req.setRequestHeader(reqh);
		req.setChangeOptionalOfferRequest(sub);
		ChangeOptionalOfferResultMsg test = service_SubscribeMgr.changeOptionalOffer(req);
	
		System.out.println("Inside OCSobjprov3.java : "+test.getResultHeader().getResultDesc());
		
		if(test.getResultHeader().getResultCode().equals("405000000")) return true;
		else return false;
			
		
		
	}
	public String prePostEligibilityCheck(String msisdn,String retrictedMpList,String restrictedSogList) throws Exception
	{
		String eleResult = "ER:DEFAULT";
		boolean eleg = true;
		String rejReason="DEFAULT";
	
		RequestHeader reqh =getHeader("QueryCustomer");
		com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest sub = new com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest();
        sub.setSubscriberNo(msisdn);
        sub.setQueryType(1);
        com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg    req = new com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg();
        req.setQueryCustomerRequest(sub);
        req.setRequestHeader(reqh);
        QueryCustomerResultMsg test = service_QueryMgr.queryCustomer(req);
						
		
		String rdisc =test.getResultHeader().getResultCode();
		if(rdisc.equals("405000000"))
		{
			////////////////////////////////
			int balance=0;
			String mpdtId="";
			String paidmd="0";
		
	
			
			//get the balance....
			for(int bal=0;bal<test.getQueryCustomerResult().getAccount(0).getBalanceRecordList().getBalance().length;bal++)
			{
				long a1 =test.getQueryCustomerResult().getAccount(0).getBalanceRecordList().getBalance(bal).getAmount();
				String a3 = test.getQueryCustomerResult().getAccount(0).getBalanceRecordList().getBalance(bal).getAccountType();
				if(a3.equals("2000"))
				{
					balance=(int)a1;
				}
			
			}
						
		
			mpdtId = String.valueOf(test.getQueryCustomerResult().getSubscriber(0).getSubscriberInfo().getPrimaryOfferId());
			paidmd = test.getQueryCustomerResult().getSubscriber(0).getSubscriberInfo().getPaidMode().toString();
			
			if(paidmd.equals("0"))
			{
				int st = test.getQueryCustomerResult().getSubscriber(0).getSubscriberState().getLifeCycleState();
				
				if(st == 0)
				{
					 eleg = false;
					 rejReason="MP_STATUS_IDLE";
				}
				else if(st == 2)
				{
					 eleg = false;
					 rejReason="MP_STATUS_INACTIV";
				}
				else if(st == 3 ||st == 4)
				{
					 eleg = false;
					 rejReason="MP_STATUS_POOL";
				}
				else
				{
					String [] rejMpLst = retrictedMpList.split(",");
					for(int i=0;i<rejMpLst.length;i++)
					{
						if(mpdtId.equals(rejMpLst[i]))
						{
							eleg = false;
							rejReason="MP_"+mpdtId+"_NOT_ELEGIBLE";
							System.out.println(rejReason);
							break;
						}
					}
					if(eleg)
					{				
						int stR=-1;
						int stP=-1;
						if(test.getQueryCustomerResult().getSubscriber(0).getProductList() != null)
						{
							for(int i=0;i < test.getQueryCustomerResult().getSubscriber(0).getProductList().length; i++)
							{
								String t_chk =String.valueOf(test.getQueryCustomerResult().getSubscriber(0).getProductList()[i].getOfferId());
								
								stP= Integer.parseInt(test.getQueryCustomerResult().getSubscriber(0).getProductList()[i].getStatus());
								//stR=qbp[i].getBillStatus();
								System.out.println("APPPROD:P_ST:R_ST|"+t_chk+":"+stP+":"+stR);
								if(stP!=0) 
								{
									eleg = false;
									rejReason="APPPROD_"+t_chk+"_IS_SUSPENDED";
									System.out.println(rejReason);
									break;
								}
								if(restrictedSogList.indexOf(t_chk) >=0)
								{
									eleg = false;
									rejReason="SUBS_WITH_APPPROD_"+t_chk+"_IS_NOT_ALLOWED";
									System.out.println(rejReason);
									break;
								}
							}
							if(eleg)
							{
															
								for(int i=0; i<test.getQueryCustomerResult().getSubscriber(0).getService().length; i++)
								{
								String a2 = String.valueOf(test.getQueryCustomerResult().getSubscriber(0).getService(i).getId());
									
									if(a2.equals("26"))
									{
										eleg = false;
										rejReason="GROUP "+a2+"_SERVICE_AVAILABLE";
										System.out.println(rejReason);
										break;
									}
									
								}
								if(eleg)
								{
									try
									{
										String sogcx = SogQuary(msisdn);
										String [] sogArray = sogcx.split(","); 
										if(sogArray[0].equals("1"))
										{
											eleg =false;
											rejReason="INCLUDED_IN_A_SOG_GN_"+sogArray[1]+"_SOGPID_"+sogArray[4];
										}
									}
									catch (Exception e){}
								}
							}
						}
					}
				}
			}
			else
			{
				eleg = false;
				rejReason="NOT_A_PREPAID";
			}
			
			if(eleg)
			{
				eleResult = "OK:ELEGIBLE_FOR_POST_CONVERTION:"+balance;
			}
			else
			{
				eleResult = "ER:"+rejReason;
			}
			///////////////////////////////

		}
		else
		{
			eleResult ="ER:"+rdisc;
		}
		return eleResult;
	}
	public boolean searchAppProduct(String msisdn, String pridstr) throws Exception
	{
		RequestHeader reqh =getHeader("QueryCustomer");
		com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest sub = new com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest();
        sub.setSubscriberNo(msisdn);
        sub.setQueryType(1);
        com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg    req = new com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg();
        req.setQueryCustomerRequest(sub);
        req.setRequestHeader(reqh);
        QueryCustomerResultMsg test = service_QueryMgr.queryCustomer(req);
		String rdisc =test.getResultHeader().getResultCode();
		boolean rslt=false;
		if(rdisc.equals("405000000"))
		{
			if(test.getQueryCustomerResult().getSubscriber(0).getProductList() !=null)
			{
				for(int i=0;i < test.getQueryCustomerResult().getSubscriber(0).getProductList().length;i++)
				{
					String t_chk =String.valueOf(test.getQueryCustomerResult().getSubscriber(0).getProductList()[i].getOfferId());
					String [] tempArr=null;
					tempArr=pridstr.split(",");
					for(int ki=0;ki<tempArr.length;ki++)
					{
						if(tempArr[ki].equals(t_chk))
						{
							rslt=true;
							break;
						}
					}
					if(rslt)
					{
						break;
					}
				}
			}
			return rslt;
			
			
		}
		
		return rslt;
		
	}
	
	public boolean sogSearch(String pnum) throws Exception
	{
		RequestHeader reqh =getHeader("QueryUserGroup");
		com.huawei.www.bme.cbsinterface.usergroupmgrt.QueryUserGroupRequest sub = new com.huawei.www.bme.cbsinterface.usergroupmgrt.QueryUserGroupRequest();
		sub.setSubscriberNo(pnum);
		com.huawei.www.bme.cbsinterface.usergroupmgrtmgr.QueryUserGroupRequestMsg req = new com.huawei.www.bme.cbsinterface.usergroupmgrtmgr.QueryUserGroupRequestMsg();
		req.setQueryUserGroupRequest(sub);
		req.setRequestHeader(reqh);
		com.huawei.www.bme.cbsinterface.usergroupmgrtmgr.QueryUserGroupResultMsg test = service_UserGroupMgrt.queryUserGroup(req);
		int j=0;
		if(test.getQueryUserGroupResult() != null)
		{
		while(j < test.getQueryUserGroupResult().length)
		{
			if(test.getQueryUserGroupResult()[j].getGroupBasicInfo().getGroupType() == 100)
			{
				return true;
			}
			
		}
		}
		
		return false;
		
		
	}
	public String SogManage(String msisdn,int action,int p) throws Exception
	{
		String pid;
		if(p==1)
		{
			pid="85008";
		}
		else if(p==0)
		{
			pid="85009";
		}
		else
		{
			pid=p+"";
		}
		
		return SogAddRemWithPID(msisdn,action,pid);
		
	}
	public String SogAddSup(String pnum,String snum,int action) throws Exception
	{
		RequestHeader reqh = getHeader("ManageUserGroupMember");
		ManageUserGroupMemberRequest sub= new  ManageUserGroupMemberRequest();
		sub.setOwnerSubscriberNo(pnum);
		sub.setOperationType(action);
		
		ManageUserGroupMemberRequestGroupInfo ugInfo = new  ManageUserGroupMemberRequestGroupInfo();
		ugInfo.setGroupType(100);
		sub.setGroupInfo(ugInfo);
		
		ManageUserGroupMemberRequestMemberInfo mgInfo = new ManageUserGroupMemberRequestMemberInfo();
		mgInfo.setSubscriberNo(snum);
		mgInfo.setMemberType(102);
		sub.setMemberInfo(mgInfo);
		
		ManageUserGroupMemberRequestMsg req = new ManageUserGroupMemberRequestMsg();
		req.setRequestHeader(reqh);
		req.setManageUserGroupMemberRequest(sub);
		ManageUserGroupMemberResultMsg test = service_UserGroupMgrt.manageUserGroupMember(req);
		return test.getResultHeader().getResultDesc();
	}
	public String subscribeApendantProductStrRet(String msisdn, String prid) throws Exception
	{
		return subdesubApendantProduct(msisdn,prid,"","",true,1);
	}
	public boolean createAccWithPmAbalApdt(String msisdn,String imsi,String pdid,String paidMode,int accbal,String apppdtid) throws Exception
	{
		
		RequestHeader reqh= getHeader("NewSubscriber");
		NewSubscriberRequest sub = new NewSubscriberRequest();

	
		com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestCustomer customer = new com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestCustomer();
		customer.setGender(Gender.value1);
		customer.setOperType(1);
		sub.setCustomer(customer);
		
		com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestAccount[] account = new com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestAccount[1];
		account[0] = new com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestAccount();
		account[0].setOperType(1);
		com.huawei.www.bme.cbsinterface.common.PaidMode pm=null;
		if(Integer.parseInt(paidMode)==0) pm=PaidMode.value1;
		else if(Integer.parseInt(paidMode)==1) pm=PaidMode.value2;
		else pm=PaidMode.value3;
		System.out.println("Paid mode  " +pm.toString());
		account[0].setPaidMode(pm);
		account[0].setBillCycleType(1);
		account[0].setTitle(4050000);
		account[0].setFirstName("");
		account[0].setLastName("-");
		account[0].setBillAddress1(msisdn);
		account[0].setPPSAcctCredit(Long.parseLong(String.valueOf(accbal)));
		sub.setAccount(account);
		
				
		com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestPrimaryOffer primaryOffer = new com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestPrimaryOffer();
		primaryOffer.setId(Integer.parseInt(pdid));
			
		
		com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestPrimaryOfferSubscriber [] subscriber = new com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestPrimaryOfferSubscriber[1];
		subscriber[0] = new com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestPrimaryOfferSubscriber();
		subscriber[0].setSubscriberNo(msisdn);
		subscriber[0].setIMSI(imsi);
		subscriber[0].setMainProductId(getMainprodId(pdid));
		subscriber[0].setPassword(msisdn);
				
		SimpleProperty [] subSimplePropertyList =new SimpleProperty[1];
		SimpleProperty  subsim = new SimpleProperty();
		subsim.setId("LangType");
		subsim.setValue("1");
		subSimplePropertyList[0] = subsim;
				
		subscriber[0].setSimpleProperty(subSimplePropertyList);
		primaryOffer.setSubscriber(subscriber);
		sub.setPrimaryOffer(primaryOffer);
		
		com.huawei.www.bme.cbsinterface.subscribe.OptionalOfferOrder[] optionalOffer;
		optionalOffer = new com.huawei.www.bme.cbsinterface.subscribe.OptionalOfferOrder[2];
		com.huawei.www.bme.cbsinterface.subscribe.OptionalOfferOrder optinal_obj = new com.huawei.www.bme.cbsinterface.subscribe.OptionalOfferOrder();
		optinal_obj.setId(Integer.parseInt(apppdtid));
		optionalOffer[0] = optinal_obj;
		optinal_obj.setId(Integer.parseInt("21"));  //Need to change the offer id. in the old , it is 71503
		optionalOffer[1] = optinal_obj;
		sub.setOptionalOffer(optionalOffer);
		
				
		NewSubscriberRequestMsg req =new NewSubscriberRequestMsg();
		req.setRequestHeader(reqh);
		req.setNewSubscriberRequest(sub);
	
		NewSubscriberResultMsg test=service_NewSubscriberMgr.newSubscriber(req);
		
		
		System.out.println(test.getResultHeader().getResultCode());
		System.out.println(test.getResultHeader().getResultDesc());
		
		if(test.getResultHeader().getResultCode().equals("405000000"))
		{
			return true;
		}
		else
		{
			return false;
		}
			
	}
	public boolean clearNegbal(String msisdn,int reasonc,int nBalance,String appDate) throws Exception
	{
		RequestHeader reqh= getHeader("AdjustNegBalance");
		AdjustNegBalanceRequest sub = new AdjustNegBalanceRequest();
		sub.setSubscriberNo(msisdn);
		AdjustNegBalance[] adjneg = new AdjustNegBalance[1];
		adjneg[0].setOpCode(reasonc);
		adjneg[0].setAdjustValue(nBalance);
		adjneg[0].setApplyTime(appDate);
		sub.setAdjustNegBalance(adjneg);
		
		AdjustNegBalanceRequestMsg req = new AdjustNegBalanceRequestMsg();
		req.setRequestHeader(reqh);
		req.setAdjustNegBalanceRequest(sub);
		
		AdjustNegBalanceResultMsg test = service_NegBalanceMgr.adjustNegBalance(req);
		
		if(test.getResultHeader().getResultCode().equals("405000000"))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}	
	public String getProductName(String pid) throws Exception
	{
		
	  Class.forName("oracle.jdbc.driver.OracleDriver");
	  String dbUrl = "jdbc:oracle:thin:@(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.71.226)(PORT = 1522))(ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.71.227)(PORT = 1522))(LOAD_BALANCE = yes)(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME =apps)))";
	  Connection con=DriverManager.getConnection(dbUrl,"taploader","taploader");
	  Statement stmt = con.createStatement();
	  //System.out.println("insert into sog_data(op_date,trans_id,pnum,snum1,snum2,flag) values ( to_date('"+dateFormat.format(cdate)+"','YYYY/MM/DD HH24:MI:SS'),"+counter+",'"+accountA+"','"+accountB+"','no',1)");											
	  ResultSet rs = stmt.executeQuery("select pname from OCS_PRODUCT where pid='"+pid+"'");											
	  if(rs.next())
	  {
	  	String pname =rs.getString(1);
	  	rs.close();
		  stmt.close();
			con.close();
	  	return pname;
	  }
	  else return pid;
	  	
	}
	public String addDaysToDate(String date, int daysToAdd)throws Exception 
	{
	
	DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	Date parsedDate = sdf.parse(date);
	Calendar now = Calendar.getInstance();
	now.setTime(parsedDate);
	now.add(Calendar.DAY_OF_MONTH, daysToAdd);
	String da = sdf.format(now.getTime());
	
	return da;
	}
	public String getProductID(String pname) throws Exception    //Need to change the relevant offer IDs in OCS 3.3
	{		
		String retProdId="0";
		String [] prodName = new String[47];
		prodName[0] = "50001,KIT";
		prodName[1] = "50002,Half Rate";
		prodName[2] = "50003,HOME_ZONE";
		prodName[3] = "50004,OLD_HZONE";
		prodName[4] = "50005,NEW_HOM_ZN";
		prodName[5] = "50008,KITME";
		prodName[6] = "50009,EZBUDDY";
		prodName[7] = "50010,Suxen-MERC";
		prodName[8] = "50011,Ezpay-MERC";
		prodName[9] = "50012,Kid-CUG";
		prodName[10] = "50013,Dtel-Gphon";
		prodName[11] = "50014,3G-Super";
		prodName[12] = "50015,SnrCtz";
		prodName[13] = "50016,DAILY_X";
		prodName[14] = "50017,DAILYX-P";
		prodName[15] = "50018,Air-Rent";
		prodName[16] = "50019,PLN-Roam";
		prodName[17] = "50020,KIT-D";
		prodName[18] = "50023,KITME-D";
		prodName[19] = "50024,SnrCtz-D";
		prodName[20] = "50025,KIT-P";
		prodName[21] = "50026,HOM_ZONE-P";
		prodName[22] = "50027,OLD_HZON-P";
		prodName[23] = "50028,NEW_HZON-P";
		prodName[24] = "50031,KITME-P";
		prodName[25] = "50032,SnrCtz-P";
		prodName[26] = "50033,Symphony-P";
		prodName[27] = "50034,KIT-A";
		prodName[28] = "50037,KITME-A";
		prodName[29] = "50038,SnrCtz-A";
		prodName[30] = "50039,TEMP-DISCNT";
		prodName[31] = "50040,North";
		prodName[32] = "50041,North-P";
		prodName[33] = "50042,North-A";
		prodName[34] = "50043,IVR_KIT";
		prodName[35] = "50044,IVR_KIT-P";
		prodName[36] = "50045,IVR_KIT-A";
		prodName[37] = "50046,Month-Package";
		prodName[38] = "50047,DialogStaff-P";
		prodName[39] = "50048,DialogStaff";
		prodName[40] = "50006,DisKIT";
		prodName[41] = "50007,DisKIT-D";
		prodName[42] = "50001,Post";//Post-A
		prodName[43] = "50025,Post-P";
		prodName[44] = "50034,Post-A";
		prodName[45] = "50020,Post-D";
		prodName[46] = "50050,UK-KIT-P";
		for(int i=0;i<47;i++)
		{
			String [] ArrT =null;
			ArrT = prodName[i].split(",");
			if(pname.equals(ArrT[1]))
			{
				retProdId=ArrT[0];
				break;
			}
		}
		return retProdId;

	}
	/*
	public int getAppProdId(String optionalOfferId) 
	{
	 int appProductId=Integer.parseInt(optionalOfferId);
	 int return_pkg=0;
			
		switch(appProductId)
		{
			case 10: 	return_pkg =81;break;  
			case 87: 	return_pkg =95;break;  
			case 3: 	return_pkg =74;break;   
			case 299: 	return_pkg =81;break;  
			default: 	return_pkg =81;break;  
		}
		
		
	 return return_pkg;
	}
	public int getMainprodId(String primaryOfferId) throws Exception 
	{
		 int appProductId=Integer.parseInt(primaryOfferId);
		 int return_pkg=0;
				
			switch(appProductId)
			{
				case 10: 	return_pkg =81;break;  
				case 179: 	return_pkg =81;break;  
				case 3: 	return_pkg =74;break;   
				case 299: 	return_pkg =81;break;  
				default: 	return_pkg =81;break;  
			}
			
			
		 return return_pkg;
	}
	*/
	public int getOCSVersion(long number) throws Exception
	{
		 /*//String numbers = (String)env.lookup("numberlevels");
		 String [] numlevels = this.numbers.split("\\|");
		 
		 //String ocs3_testing_numbers = (String)env.lookup("ocs3_testing_numbers");
		 String [] testing_numbers = this.ocs3_testing_numbers.split("\\|");
		 int version=1;
		 boolean testNum=false;
		 for(int j=0;j< testing_numbers.length ;j++)
		 {
			 if(Long.parseLong(testing_numbers[j])== number)
			 {
				 version =  3;
				 testNum=true;
				 break;
			 }
		 }
		 if(!testNum)
		 {
			 for(int i=0;i< numlevels.length ;i++)
			 {
				 String [] lv = numlevels[i].split(",");
				 if(number >= Long.parseLong(lv[1]) && number <=Long.parseLong(lv[2]))
				 {
					 version =  Integer.parseInt(lv[0]);
					 break;
				 }
			 }
		 }
		 */
		 return 3;
	}
  public int getMainprodId(String primaryOfferId) throws Exception 
	{
		 //String po2mp = (String)env.lookup("po2pm");
		 String [] po2mpList = this.po2mp.split("\\|");
		 int mainProductId=Integer.parseInt(primaryOfferId);
		 for(int i=0;i< po2mpList.length ;i++)
		 {
			 String [] lv = po2mpList[i].split(",");
			 if(lv[0].equals(primaryOfferId))
			 {
				 mainProductId =  Integer.parseInt(lv[1]);
				 break;
			 }
		 }
		 return mainProductId;
	}
	public int getAppProdId(String optionalOfferId) throws Exception 
	{
		//String oo2ap = (String)env.lookup("oo2ap");
		String [] oo2apList = this.oo2ap.split("\\|");
		 int appProductId=Integer.parseInt(optionalOfferId);
		 for(int i=0;i< oo2apList.length ;i++)
		 {
			 String [] lv = oo2apList[i].split(",");
			 if(lv[0].equals(optionalOfferId))
			 {
				 appProductId =  Integer.parseInt(lv[1]);
				 break;
			 }
		 }
		 return appProductId;
	}
	public int getTariffOfferID(int mpId,int tarifMode) throws Exception 
	{
		//String mp2tap = (String)env.lookup("mp2tap");
		String [] mp2tapList = this.mp2tap.split("\\|");
		 int apptariffProductId=0;
		 for(int i=0;i< mp2tapList.length ;i++)
		 {
			 String [] lv = mp2tapList[i].split(",");
			 if(Integer.parseInt(lv[0])==mpId)
			 { 
				 apptariffProductId =  Integer.parseInt(lv[1+tarifMode]);
				 break;
			 }
		 }
		System.out.println("Tariff Product ID="+apptariffProductId);
		return apptariffProductId;
	}
	public int getFafOldOfferID(int mpId,int tarifMode) throws Exception 
	{
		//String mp2tap = (String)env.lookup("mp2tap");
		String [] mp2tapList = this.mp2fafold.split("\\|");
		 int apptariffProductId=0;
		 for(int i=0;i< mp2tapList.length ;i++)
		 {
			 String [] lv = mp2tapList[i].split(",");
			 if(Integer.parseInt(lv[0])==mpId)
			 { 
				 apptariffProductId =  Integer.parseInt(lv[1+tarifMode]);
				 break;
			 }
		 }
		System.out.println("OLD Faf Product ID="+apptariffProductId);
		return apptariffProductId;
	}
	public int getFafOfferID(int mpId,int tarifMode) throws Exception 
	{
		//String mp2tap = (String)env.lookup("mp2tap");
		String [] mp2tapList = this.mp2faf.split("\\|");
		 int apptariffProductId=0;
		 for(int i=0;i< mp2tapList.length ;i++)
		 {
			 String [] lv = mp2tapList[i].split(",");
			 if(Integer.parseInt(lv[0])==mpId)
			 { 
				 apptariffProductId =  Integer.parseInt(lv[1+tarifMode]);
				 break;
			 }
		 }
		System.out.println("FAFProduct ID="+apptariffProductId);
		return apptariffProductId;
	}
	public int getSogOldOfferID(int mpId,int tarifMode) throws Exception 
	{
		//String mp2tap = (String)env.lookup("mp2tap");
		String [] mp2tapList = this.mp2sogold.split("\\|");
		 int apptariffProductId=0;
		 for(int i=0;i< mp2tapList.length ;i++)
		 {
			 String [] lv = mp2tapList[i].split(",");
			 if(Integer.parseInt(lv[0])==mpId)
			 { 
				 apptariffProductId =  Integer.parseInt(lv[1+tarifMode]);
				 break;
			 }
		 }
		System.out.println("OLD SOG Product ID="+apptariffProductId);
		return apptariffProductId;
	}
	public int getSogOfferID(int mpId,int tarifMode) throws Exception 
	{
		//String mp2tap = (String)env.lookup("mp2tap");
		String [] mp2tapList = this.mp2sog.split("\\|");
		 int apptariffProductId=0;
		 for(int i=0;i< mp2tapList.length ;i++)
		 {
			 String [] lv = mp2tapList[i].split(",");
			 if(Integer.parseInt(lv[0])==mpId)
			 { 
				 apptariffProductId =  Integer.parseInt(lv[1+tarifMode]);
				 break;
			 }
		 }
		System.out.println("SOG Product ID="+apptariffProductId);
		return apptariffProductId;
	}
	public boolean replaceApendantProduct(String msisdn, String newprid, String oldprid) throws Exception
	{
		RequestHeader reqh= getHeader("ChangeOptionalOffer");
		ChangeOptionalOfferRequest sub = new ChangeOptionalOfferRequest();
		sub.setSubscriberNo(msisdn);
		ChangeOptionalOfferRequestOptionalOffer[] opffer =  new ChangeOptionalOfferRequestOptionalOffer[2];
		
		
		opffer[0]  = new ChangeOptionalOfferRequestOptionalOffer();	
		opffer[0].setId(Integer.parseInt(newprid));
		opffer[0].setOperationType(1);	
		EffectMode em = new EffectMode();
		em.setMode(EffectModeMode.value1);
		opffer[0].setValidMode(em);
		
		opffer[1]  = new ChangeOptionalOfferRequestOptionalOffer();	
		opffer[1].setId(Integer.parseInt(oldprid));
		opffer[1].setOperationType(2);
		opffer[1].setValidMode(em);
		
				
		sub.setOptionalOffer(opffer);
		ChangeOptionalOfferRequestMsg req = new ChangeOptionalOfferRequestMsg();
		req.setRequestHeader(reqh);
		req.setChangeOptionalOfferRequest(sub);
		ChangeOptionalOfferResultMsg test = service_SubscribeMgr.changeOptionalOffer(req);
	
		System.out.println("Inside OCSobjprov3.java : "+test.getResultHeader().getResultDesc());
		
		if(test.getResultHeader().getResultCode().equals("405000000")) return true;
		else return false;
			
		
		
	}
	public String createNewDumySubscriber(String msisdn,int paidMode,String imsi, String appName,String appIp) throws Exception
	//Create Accounts [0=prepaid, 1= postpaid, 2=highbridge]
	{
		String logmsg = "NewSubscriber|ACC:"+msisdn+"|";
		RequestHeader reqh= getHeader("NewSubscriber");
		NewSubscriberRequest sub = new NewSubscriberRequest();

		com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestCustomer customer = new com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestCustomer();
		com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestAccount[] account = new com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestAccount[1];
		com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestPrimaryOffer primaryOffer = new com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestPrimaryOffer();
		com.huawei.www.bme.cbsinterface.subscribe.OptionalOfferOrder[] optionalOffer = new com.huawei.www.bme.cbsinterface.subscribe.OptionalOfferOrder[1];
		com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestPrimaryOfferSubscriber [] subscriber = new com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestPrimaryOfferSubscriber[1];
		subscriber[0] = new com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestPrimaryOfferSubscriber();
		//customer.setOperType(1);//Create New Customer
		//subscriber.setRefereeSubNo(msisdn);
		account[0] = new com.huawei.www.bme.cbsinterface.newsubscriber.NewSubscriberRequestAccount();
		account[0].setOperType(1);
		account[0].setBillCycleType(1);
		account[0].setPaidMode(PaidMode.value1);
		int mainProductId=50001;
		if(paidMode!=0)//NOT prepaid 
		{
			mainProductId=1050001;
			account[0].setPaidMode(PaidMode.value2);
		}
		
		primaryOffer.setId(mainProductId);
		subscriber[0].setSubscriberNo(msisdn);
		subscriber[0].setIMSI(imsi);
		subscriber[0].setMainProductId(getMainprodId(mainProductId+""));
		//subscriber[0].setPassword(msisdn);
		if(paidMode==0)
		{
			optionalOffer[0] = new com.huawei.www.bme.cbsinterface.subscribe.OptionalOfferOrder();
			optionalOffer[0].setId(getTariffOfferID(getMainprodId(mainProductId+""),0));
			sub.setOptionalOffer(optionalOffer);
		}

		
		
		sub.setCustomer(customer);
		primaryOffer.setSubscriber(subscriber);
		sub.setPrimaryOffer(primaryOffer);
		sub.setAccount(account)	;
		
		NewSubscriberRequestMsg req =new NewSubscriberRequestMsg();
		req.setRequestHeader(reqh);
		req.setNewSubscriberRequest(sub);
		NewSubscriberResultMsg test=service_NewSubscriberMgr.newSubscriber(req);		
		String result2 = test.getResultHeader().getResultCode();//+"|"+test.getResultHeader().getResultDesc();
		//CommonResponseMsg comresmsg = new CommonResponseMsg(test.getResultHeader().getResultCode(),test.getResultHeader().getResultDesc());
		//logger.debug(logmsg+result2);
		return result2;

	}
	
	
	public String  queryQuota(String msisdn) throws Exception 
	{		
		String ret_str = "\nRs. 0.00";
		int quota_count=0;
		boolean service=false;
		long quota_value = 0;
		String date1="";
				
		RequestHeader reqh = getHeader("QuerySubQuota");
		QuerySubQuotaRequest quotareq = new QuerySubQuotaRequest();
		quotareq.setMsisdn(msisdn);
		QuerySubQuotaRequestMsg msg = new QuerySubQuotaRequestMsg();
		msg.setQuerySubQuotaRequest(quotareq);
		msg.setRequestHeader(reqh);
		QuerySubQuotaResultMsg test = service_masubQuotaMgr.querySubQuota(msg);
		
		System.out.println(test.getResultHeader().getResultDesc());
		
		int k = 0;
		if (test.getQuerySubQuotaResult() != null) 
		{
			while (k < test.getQuerySubQuotaResult().length) 
			{
				
				if (test.getQuerySubQuotaResult()[k].getQuotaType() == 7108) 
				{ 
					quota_value = test.getQuerySubQuotaResult()[k].getAmount();
					date1 = test.getQuerySubQuotaResult()[k].getApplyTime();
					service=true;
					int j = 0;
					if (test.getQuerySubQuotaResult()[k].getNotifyNode() != null)
					{
						ret_str="\n"; 
						while (j < test.getQuerySubQuotaResult()[k].getNotifyNode().length) 
						{
							ret_str = ret_str +"Rs "+test.getQuerySubQuotaResult()[k].getNotifyNode()[j].getLimitAmt()/10000+".00";
							
							j++;
							if(j < test.getQuerySubQuotaResult()[k].getNotifyNode().length)
							{
							ret_str = 	ret_str +"\n";
							}
						}
						quota_count = j;
						break;
					}
				}

				k++;
			}

		}
		
		
		
		if(service)
		{
			 System.out.println("CX ALERT:"+ret_str);
			return quota_value+","+quota_count+","+date1+","+ret_str;
		}
		else
		{
			return "NO Service";
		}
	
	}
		
	public String addquota(String num , long value , int op , boolean tr , String dd) throws Exception
	{
		
		/* opr = 0   View
		 * opr = 1 	 add
		 * opr = 2   Delete
		 * opr = 3   Deactivate
		 */
		
		System.out.println(num+"|"+value+"|"+op+"|"+tr+"|"+dd);
		
		
		RequestHeader reqadd = getHeader("ManSubQuota");
		ManSubQuotaRequest addreq = new ManSubQuotaRequest();
		if(op < 2)
		{	
		addreq.setAmount(value*10000);
		}
		
		if((op == 3) || (op == 1))
		{
		addreq.setApplyTime(dd);
		}
		addreq.setMsisdn(num);
		addreq.setQuotaType(7108);
		addreq.setOperType(op);
		addreq.setNotifyType(1);
		
		if(tr)
		{
		ManSubQuotaRequestNotifyNode[] notifynode = new ManSubQuotaRequestNotifyNode[1];
		ManSubQuotaRequestNotifyNode  notifynodeobj = new ManSubQuotaRequestNotifyNode();
		notifynodeobj.setLimitAmt(value*10000);
		notifynode[0] = notifynodeobj;
		addreq.setNotifyNode(notifynode);
		}
		
		ManSubQuotaRequestMsg addmsg = new ManSubQuotaRequestMsg();
		addmsg.setRequestHeader(reqadd);
		addmsg.setManSubQuotaRequest(addreq);
		ManSubQuotaResultMsg addtest =  service_masubQuotaMgr.manSubQuota(addmsg);
		
		
		System.out.println(addtest.getResultHeader().getResultDesc());
		
		return addtest.getResultHeader().getResultCode();
			
	}
	
	public String getAllSubaccounts(String msisdn) throws Exception
	{
		RequestHeader reqh =getHeader("QueryBalance");
		QueryBalanceRequest sub = new QueryBalanceRequest();
		sub.setSubscriberNo(msisdn);
		QueryBalanceRequestMsg req = new QueryBalanceRequestMsg();
		req.setQueryBalanceRequest(sub);
		req.setRequestHeader(reqh);
		QueryBalanceResultMsg test = new QueryBalanceResultMsg();
		try
		{
	   test = service_QueryMgr.queryBalance(req);
		}
		catch(Exception e)
		{
			return "1,"+test.getResultHeader().getResultCode();
		}
		
		System.out.println(test.getResultHeader().getResultCode());  
		if(test.getResultHeader().getResultCode().equals("405000000"))
		{
					
		int k=0;
		String ret_str="";
		while( k < test.getQueryBalanceResult().getBalanceRecord().length)
		{
		
		ret_str = ret_str +","+test.getQueryBalanceResult().getBalanceRecord(k).getAccountType();
		
		k++;
		}
		
		ret_str =ret_str.trim();
		return "0"+ret_str;			
				
		}
		else
		{
			return "1,"+test.getResultHeader().getResultDesc();
		}
			
		
	}
	
	public String checkConvergent(String msisdn) throws Exception
	{
		RequestHeader reqh =getHeader("QueryCustomer");
		com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest subq = new com.huawei.www.bme.cbsinterface.query.QueryCustomerRequest();
		subq.setSubscriberNo(msisdn);
		subq.setQueryType(1);
		com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg req = new com.huawei.www.bme.cbsinterface.querymgr.QueryCustomerRequestMsg();
		req.setQueryCustomerRequest(subq);
		req.setRequestHeader(reqh);
		QueryCustomerResultMsg test =  service_QueryMgr.queryCustomer(req);
											
		if(test.getResultHeader().getResultDesc().equals("Operation successfully."))
		{
		
		int k=0;
		boolean checkMP=false; 
		
		while(k < test.getQueryCustomerResult().getSubscriber().length)
		{
		  if(test.getQueryCustomerResult().getSubscriber(k).getSubscriberInfo().getPrimaryOfferId() == 19820020)
		  {
		   	checkMP=true;
		  	break;
		  }
		  k++;
		}
		
	String cnv_info=null;
	String masterNum=null;
	boolean chk_prod=false;
	  if(checkMP)
	  {
	  	
	  	cnv_info = test.getQueryCustomerResult().getCustomer().getCustomerInfo().getRemark();
	  	if(cnv_info!=null)
	  	{
	  		 String [] cnv_info_A=cnv_info.split("_");
	  		  if(cnv_info_A !=null)
		        {
		        	if(cnv_info_A[0].compareToIgnoreCase("MAS")==0)
		        	{
		        			masterNum=cnv_info_A[1];
		        	}
		        }
		        
		        String prod ="1103051,1103053,1103052,";
		        if(masterNum.equals(msisdn))
		        {
		        	        
		        chk_prod = searchAppProduct(msisdn,prod);
		        if(chk_prod)
		        {
		        	return "1|MAS|OK|"+msisdn;
		        }
		        else
		        {
		        	return "1|MAS|NOK|";
		        }
		        
		        
		        }
		        else
		        {
		        	chk_prod = searchAppProduct(masterNum,prod);
		        	 if(chk_prod)
		        {
		        	return "1|MEM|OK|"+masterNum;
		        }
		        else
		        {
		        	return "1|MEM|NOK|";
		        }
		        }
		        
		        
		        
	  	}
	  	else
	  	{
	  	return "1|RE:"+cnv_info+"|";		
	  	}
	  	
	  	
	  	
	  }
	  else
	  {
	  return "0|NOTCONV";	
	  }
	
	
		
		}
		else
		{
			return "0|"+test.getResultHeader().getResultDesc();
	
		}
		
		
		
		
	}
	
	public String getAllSubaccountsWithBoundProd(String msisdn , String ACCTYPE) throws Exception
	{
		RequestHeader reqh =getHeader("QueryBalance");
		QueryBalanceRequest sub = new QueryBalanceRequest();
		sub.setSubscriberNo(msisdn);
		QueryBalanceRequestMsg req = new QueryBalanceRequestMsg();
		req.setQueryBalanceRequest(sub);
		req.setRequestHeader(reqh);
		QueryBalanceResultMsg test = new QueryBalanceResultMsg();
		try
		{
	   test = service_QueryMgr.queryBalance(req);
		}
		catch(Exception e)
		{
			return "1,"+test.getResultHeader().getResultCode();
		}
		
		System.out.println(test.getResultHeader().getResultCode());  
		if(test.getResultHeader().getResultCode().equals("405000000"))
		{
					
		int k=0;
		String ret_str="";
		while( k < test.getQueryBalanceResult().getBalanceRecord().length)
		{
			if(ACCTYPE.trim().equals("") || ACCTYPE.equals(test.getQueryBalanceResult().getBalanceRecord(k).getAccountType()))
			{
				ret_str = ret_str +","+test.getQueryBalanceResult().getBalanceRecord(k).getAccountType()+"|"+test.getQueryBalanceResult().getBalanceRecord(k).getRelatedProductID()+"|"+test.getQueryBalanceResult().getBalanceRecord(k).getAmount()+"|"+test.getQueryBalanceResult().getBalanceRecord(k).getExpireTime()+"|";	;
			}
		
		k++;
		}
		
		ret_str =ret_str.trim();
		return "0"+ret_str;			
				
		}
		else
		{
			return "1,"+test.getResultHeader().getResultDesc();
		}
			
		
	}

}
